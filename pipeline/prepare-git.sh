#!/bin/bash

set -e
set -u

# Simple script to setup git, expects variables:
# CI_SSH_PRIVATE_KEY with base64 encoded private key - $(cat id_rsa | base64 -w0)

mkdir -p ~/.ssh
chmod 700 ~/.ssh
ssh-keyscan -H gitlab.com >> ~/.ssh/known_hosts 2>/dev/null


if [ "${CI_SSH_PRIVATE_KEY:-}" != "" ]; then
    echo "$CI_SSH_PRIVATE_KEY" | base64 -d > ~/.ssh/id_rsa
    chmod 400 ~/.ssh/id_rsa
fi

REPO_URL=`echo $CI_PROJECT_URL | sed -re 's#^http(s)?://([^/]+)/(.*)#git@\2:\3.git#'`

git config --global user.email "gitlab@gitlab.com"
git config --global user.name "GitLab Pipeline"
git remote set-url origin $REPO_URL

set -x
git fetch
git checkout $CI_COMMIT_REF_NAME
git reset --hard origin/$CI_COMMIT_REF_NAME


package kraken.api

import com.squareup.moshi.JsonAdapter
import kotlinx.coroutines.Job
import kraken.api.http.data.Asset
import kraken.api.http.data.AssetPair
import kraken.api.http.data.OrderBook
import kraken.api.http.data.response.GetAssetPairsResponse
import kraken.api.http.data.response.GetAssetsResponse
import kraken.api.http.data.response.OrderBookResponse
import kraken.api.infra.moshi
import kraken.api.infra.schedule
import kraken.api.infra.scheduleOnce
import kraken.api.infra.verifyApiResponseErrors
import kraken.api.infra.verifyHttpStatusResponse
import kraken.api.infra.toUrlQueryString
import kraken.api.ws.BaseWsClient
import kraken.api.ws.Channel
import kraken.api.ws.MessageHandler
import kraken.api.ws.PongHandler
import kraken.api.ws.SystemStatusHandler
import kraken.api.ws.SystemStatusHandlerImpl
import kraken.api.ws.TickerHandler
import kraken.api.ws.data.PingPongMessage
import kraken.api.ws.data.SubscribeRequest
import kraken.api.ws.data.SystemStatusMessage
import kraken.api.ws.data.TickerMessage
import org.http4k.client.OkHttp
import org.http4k.client.WebsocketClient
import org.http4k.core.Method.GET
import org.http4k.core.Request
import org.http4k.core.Uri
import org.http4k.websocket.Websocket
import org.http4k.websocket.WsMessage
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.Duration
import java.time.Instant
import java.util.concurrent.atomic.AtomicReference

/**
 * Public endpoints - https://www.kraken.com/features/api#public-market-data
 */
class KrakenPublicClientImpl(
    httpUrl: String,
    val webSocketUrl: String,
    val connectionCheckInterval: Duration = Duration.ofSeconds(60),
    val connectionCheckTimeout: Duration = Duration.ofSeconds(5),
    val reconnectAttemptInterval: Duration = Duration.ofSeconds(140),
) : KrakenPublicClient, BaseWsClient() {

    /**
     * API endpoints
     * httpUrl expected to be hostname: https://api.kraken.com
     */
    private val ASSETS = "$httpUrl/0/public/Assets"
    private val ASSET_PAIRS = "$httpUrl/0/public/AssetPairs"
    private val ORDER_BOOK_URL = "$httpUrl/0/public/Depth"

    /**
     * Adapters for serialization
     */
    private val getAssetsAdapter: JsonAdapter<GetAssetsResponse> = moshi.adapter(GetAssetsResponse::class.java)
    private val getAssetPairsAdapter: JsonAdapter<GetAssetPairsResponse> = moshi.adapter(GetAssetPairsResponse::class.java)
    private val orderBookAdapter: JsonAdapter<OrderBookResponse> = moshi.adapter(OrderBookResponse::class.java)

    /**
     * Websocket related.
     */
    private var subscribedRequests: List<SubscribeRequest> = emptyList()
    private var websocket: Websocket? = null
    // base collection of "internal" handlers
    private var handlers: List<MessageHandler<*>> = listOf(SystemStatusHandlerImpl, PongHandlerImpl)
    // connection checker
    private var connectionCheck: Job? = null
    private val lastReconnectAttempt: AtomicReference<Instant> = AtomicReference(Instant.now().minus(reconnectAttemptInterval))

    private val http by lazy {
        OkHttp()
    }

    override fun getAssets() : List<Asset> = http(Request(GET, ASSETS))
                .also { verifyHttpStatusResponse(it, log) }
                .let { getAssetsAdapter.fromJson(it.bodyString())!! }
                .also { verifyApiResponseErrors(it, log) }
                // mapping to Asset
                .let { it.result!! }.toList().map {
                    Asset(it.first, it.second.altname, it.second.decimals, it.second.display_decimals)
                }

    override fun getAssetPairs(): List<AssetPair> = http(Request(GET, ASSET_PAIRS))
                .also { verifyHttpStatusResponse(it, log) }
                .let { getAssetPairsAdapter.fromJson(it.bodyString())!! }
                .also { verifyApiResponseErrors(it, log) }
                .let { it.mapToAssetPairList(it) }

    override fun getOrderBook(assetCode: String): OrderBook =
                http(Request(GET, ORDER_BOOK_URL.withQueryParams(mapOf("pair" to assetCode))))
                    .also { verifyHttpStatusResponse(it, log) }
                    .let { orderBookAdapter.fromJson(it.bodyString())!! }
                    .also { verifyApiResponseErrors(it, log) }
                    .mapToOrderBook()

    override fun subscribe(
        channels: Set<Channel>,
        handlers: Collection<MessageHandler<*>>,
        assetPairs: Collection<String>
    ) {

        // close if active
        websocket?.close()

        // handlers
        if (handlers.isEmpty()) log.warn("Empty list of handlers, probably not configured properly!")
        this.handlers = this.handlers.plus(handlers)

        // subscription list
        subscribedRequests = channels.map {
            SubscribeRequest(
                event = SUBSCRIBE,
                pair = assetPairs,
                subscription = SubscribeRequest.Subscription(name = it.channelName))
        }

        log.info("Will connect websocket, with subscriptions: $subscribedRequests")
        connect(subscribedRequests)
    }

    private fun connect(subscribed: List<SubscribeRequest>) {
        // verify time since last reconnect
        val lastReconnectInstant = lastReconnectAttempt.get()
        val now = Instant.now()
        if (now.isBefore(lastReconnectInstant.plus(reconnectAttemptInterval))) {
            log.trace("Will wait, lastReconnect occurred $lastReconnectInstant")
            return
        }

        // ok we are some time after the last attempt
        if (lastReconnectAttempt.compareAndSet(lastReconnectInstant, now)) {
            log.info("Will reconnect PUBLIC websocket")
            websocket = WebsocketClient.nonBlocking(uri = Uri.of(webSocketUrl)) { ws ->
                ws.onMessage { messageHandler(it.bodyString()) }
                ws.onError { log.error("Websocket error: ", it) }
                ws.onClose {
                    log.warn("Websocket closed: ${it.description}. Reconnect will be triggered by next PING req...")
                }
                /* send subscriptions */
                subscribed.forEach { ws.send(WsMessage(SubscribeRequest.jsonAdapter.toJson(it))) }

                /* connection checker */
                connectionCheck?.cancel() // cancel previous
                connectionCheck = scheduleConnectionCheck(ws)
            }
        } else {
            log.info("Unable to set lastReconnectAttempt time, probably race conditions during reconnect of Public Websocket.")
        }
    }

    private fun messageHandler(payload: String) {
        log.trace("Received message : {}", payload)

        when (val channel = resolveChannel(payload)) {
            Channel.SystemStatus -> handlers.filterIsInstance<SystemStatusHandler>().forEach { it.handleMessage(SystemStatusMessage.toKotlinObject(payload)) }
            Channel.Ticker -> handlers.filterIsInstance<TickerHandler>().forEach { it.handleMessage(TickerMessage.toKotlinObject(payload)) }
            Channel.Pong -> handlers.filterIsInstance<PongHandler>().forEach { it.handleMessage(PingPongMessage.toKotlinObject(payload)) }
            else -> log.trace("Undefined handler for $channel") // ok, just log
        }
    }

    private fun scheduleConnectionCheck(ws: Websocket) = schedule(interval = connectionCheckInterval) {
        // send ping with timestamp
        val stamp = System.currentTimeMillis()
        log.trace("Will send ping request with stamp {}", stamp)
        val pingReq = PingPongMessage.jsonAdapter.toJson(PingPongMessage(event = PingPongMessage.PING, reqid = stamp))

        // send with reconnect check
        if (sendWsReqWithReconnectCheck(ws, pingReq) { reconnectWebsocket(ws, "[PING] Unable to send ping request") }) {
            log.trace("Request sent, will wait for response")

            // wait for response (pong)
            scheduleOnce(delay = connectionCheckTimeout) {
                val receivedPong = PongHandlerImpl.receivedPong
                log.trace("Received pong reqId: {} expected : {}", receivedPong?.reqid, stamp)

                // verify in response is expected timestamp
                if (stamp != receivedPong?.reqid) {
                    reconnectWebsocket(ws, "[PING] No response for ping")
                }
            }
        }
    }

    private fun reconnectWebsocket(ws: Websocket, message: String) {
        log.warn("Reconnect public websocket is needed, due to: $message")
        ws.close()
        connect(subscribedRequests)
    }

    private fun String.withQueryParams(params: Map<String, String>) =
        if (params.isNullOrEmpty()) this else this + "?" + params.toUrlQueryString()

    object PongHandlerImpl : PongHandler {
        var receivedPong: PingPongMessage? = null

        override fun handleMessage(message: PingPongMessage) {
            receivedPong = message
        }
    }

    companion object {
        val log: Logger = LoggerFactory.getLogger(KrakenPublicClientImpl::class.java)
        const val SUBSCRIBE = "subscribe"
        const val RECONNECT_DELAY_MS = 10_000L
    }
}

package kraken.api

import com.google.common.cache.CacheBuilder
import com.google.common.hash.Hashing
import com.google.common.io.BaseEncoding
import com.squareup.moshi.JsonAdapter
import kotlinx.coroutines.Job
import kraken.api.domain.OrderInfo
import kraken.api.domain.TradeInfo
import kraken.api.domain.TradeVolumeInfo
import kraken.api.http.data.Balance
import kraken.api.http.data.response.BalancesResponse
import kraken.api.http.data.ClosedOrdersFilter
import kraken.api.http.data.QueryOrdersFilter
import kraken.api.http.data.QueryTradesFilter
import kraken.api.http.data.response.GetWebsocketTokenResponse
import kraken.api.http.data.TradeBalance
import kraken.api.http.data.TradeHistoryFilter
import kraken.api.http.data.WebSocketToken
import kraken.api.http.data.response.ClosedOrdersResponse
import kraken.api.http.data.response.QueryOrdersResponse
import kraken.api.http.data.response.QueryTradesResponse
import kraken.api.http.data.response.TradeBalanceResponse
import kraken.api.http.data.response.TradeHistoryResponse
import kraken.api.http.data.response.TradeVolumeResponse
import kraken.api.http.exception.NotSubscribedException
import kraken.api.http.exception.RateLimitExceeded
import kraken.api.infra.moshi
import kraken.api.infra.schedule
import kraken.api.infra.scheduleOnce
import kraken.api.infra.toUrlQueryString
import kraken.api.infra.verifyApiResponseErrors
import kraken.api.infra.verifyHttpStatusResponse
import kraken.api.ws.AddOrderStatusHandler
import kraken.api.ws.BaseRequestReplyMessageHandler
import kraken.api.ws.BaseWsClient
import kraken.api.ws.CancelOrderStatusHandler
import kraken.api.ws.Channel
import kraken.api.ws.HeartbeatHandler
import kraken.api.ws.HeartbeatHandlerImpl
import kraken.api.ws.MessageHandler
import kraken.api.ws.OpenOrdersHandler
import kraken.api.ws.OwnTradesHandler
import kraken.api.ws.PongHandler
import kraken.api.ws.SubscriptionStatusHandler
import kraken.api.ws.SubscriptionStatusHandlerImpl
import kraken.api.ws.SystemStatusHandler
import kraken.api.ws.SystemStatusHandlerImpl
import kraken.api.ws.data.AddOrder
import kraken.api.ws.data.AddOrderRequest
import kraken.api.ws.data.AddOrderStatusMessage
import kraken.api.ws.data.CancelOrder
import kraken.api.ws.data.CancelOrderRequest
import kraken.api.ws.data.CancelOrderStatusMessage
import kraken.api.ws.data.HeartbeatMessage
import kraken.api.ws.data.OpenOrdersMessage
import kraken.api.ws.data.OrderStatus
import kraken.api.ws.data.OwnTradesMessage
import kraken.api.ws.data.PingPongMessage
import kraken.api.ws.data.SubscribeRequest
import kraken.api.ws.data.SubscriptionStatusMessage
import kraken.api.ws.data.SystemStatusMessage
import org.http4k.client.OkHttp
import org.http4k.client.WebsocketClient
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Uri
import org.http4k.websocket.Websocket
import org.http4k.websocket.WsMessage
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.Duration
import java.time.Instant
import java.time.LocalDateTime
import java.util.UUID
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicReference

class KrakenPrivateClientImpl (
    baseUrl: String,
    val webSocketUrl: String,
    apiKey: String,
    privateKey: String,
    rateLimit: KrakenPrivateClient.RateLimit = KrakenPrivateClient.RateLimit(15, 3),
    requestReplyTimeout: Duration = Duration.ofSeconds(10),
    val connectionCheckInterval: Duration = Duration.ofSeconds(20),
    val connectionCheckTimeout: Duration = Duration.ofSeconds(5),
    val reconnectAttemptInterval: Duration = Duration.ofSeconds(120),
    val id: String = "0",
    krakenMetricsConsumer: KrakenPrivateClient.KrakenMetricsConsumer = NoOpKrakenMetricsConsumer,
    val heartbeatTimeout: Duration = Duration.ofMinutes(5)
) : KrakenPrivateClient, BaseWsClient() {
    // custom logger
    private val log: Logger = LoggerFactory.getLogger(javaClass.name + "(id=$id)")
    // http client
    private val httpClient = PrivateHttpClient(log, baseUrl, apiKey, privateKey, rateLimit, krakenMetricsConsumer)

    // websocket related
    private var subscribedRequests: List<SubscribeRequest> = emptyList()
    private var websocket: Websocket? = null
    // base collection of "internal" handlers
    private val addOrderStatusHandlerImpl = AddOrderStatusHandlerImpl(requestReplyTimeout)
    private val cancelOrderStatusHandlerImpl = CancelOrderStatusHandlerImpl(requestReplyTimeout)
    private val pongHandlerImpl = PongHandlerImpl()
    private val heartbeatHandler = HeartbeatHandlerImpl()
    private var handlers: List<MessageHandler<*>> = listOf(
        SystemStatusHandlerImpl, pongHandlerImpl, heartbeatHandler, SubscriptionStatusHandlerImpl,
        addOrderStatusHandlerImpl, cancelOrderStatusHandlerImpl
    )
    // connection checker
    private var connectionCheck: Job? = null
    private val lastReconnectAttempt: AtomicReference<Instant> = AtomicReference(Instant.now().minus(reconnectAttemptInterval))

    // timestampSupplier - aka source of random reqid
    private val timestampSupplier: () -> Long = { System.currentTimeMillis() }

    // serializers
    private val getWebsocketTokenAdapter: JsonAdapter<GetWebsocketTokenResponse> = moshi.adapter(GetWebsocketTokenResponse::class.java)
    private val balancesResponseAdapter: JsonAdapter<BalancesResponse> = moshi.adapter(BalancesResponse::class.java)
    private val tradeBalanceResponseAdapter: JsonAdapter<TradeBalanceResponse> = moshi.adapter(TradeBalanceResponse::class.java)
    private val closedOrdersAdapter: JsonAdapter<ClosedOrdersResponse> = moshi.adapter(ClosedOrdersResponse::class.java)
    private val queryOrdersResponseAdapter: JsonAdapter<QueryOrdersResponse> = moshi.adapter(QueryOrdersResponse::class.java)
    private val queryTradesResponseAdapter: JsonAdapter<QueryTradesResponse> = moshi.adapter(QueryTradesResponse::class.java)
    private val tradeHistoryResponseAdapter: JsonAdapter<TradeHistoryResponse> = moshi.adapter(TradeHistoryResponse::class.java)
    private val tradeVolumeResponseAdapter: JsonAdapter<TradeVolumeResponse> = moshi.adapter(TradeVolumeResponse::class.java)

    override fun getAccountBalances(): Collection<Balance> {
        return httpClient.queryPrivateApi(Operation.ACCOUNT_BALANCE)
                .let { balancesResponseAdapter.fromJson(it.bodyString())!! }
                .also { verifyApiResponseErrors(it, log) }
                .result?.entries?.map { Balance(it.key, it.value) } ?: emptyList()
    }

    override fun getTradeBalance(assetCode: String): TradeBalance {
        val assetMap = mapOf("asset" to assetCode)
        return httpClient.queryPrivateApi(Operation.TRADE_BALANCE, assetMap)
                .let { tradeBalanceResponseAdapter.fromJson(it.bodyString())!! }
                .also { verifyApiResponseErrors(it, log) }
                .result
    }

    override fun getWebSocketToken(): WebSocketToken {
        return httpClient.queryPrivateApi(Operation.GET_WEBSOCKET_TOKEN)
                .let { getWebsocketTokenAdapter.fromJson(it.bodyString())!! }
                .also { verifyApiResponseErrors(it, log) }
                .result
    }

    override fun getClosedOrders(filter: ClosedOrdersFilter): ClosedOrdersResponse {
        return httpClient.queryPrivateApi(Operation.CLOSED_ORDERS, filter)
                .let { closedOrdersAdapter.fromJson(it.bodyString())!! }
    }

    override fun queryOrdersInfo(filter: QueryOrdersFilter): Map<String, OrderInfo> {
        return httpClient.queryPrivateApi(Operation.QUERY_ORDERS, QueryOrdersFilter.toPayloadObject(filter))
                .let { queryOrdersResponseAdapter.fromJson(it.bodyString())!! }
                .also { verifyApiResponseErrors(it, log) }
                .result!!
    }

    override fun tradesHistory(filter: TradeHistoryFilter): Map<String, TradeInfo> {
        return httpClient.queryPrivateApi(Operation.TRADES_HISTORY, TradeHistoryFilter.toPayloadObject(filter))
            .let { tradeHistoryResponseAdapter.fromJson(it.bodyString())!! }
            .also { verifyApiResponseErrors(it, log) }
            .toDomainObject()
    }

    override fun queryTradesInfo(filter: QueryTradesFilter): Map<String, TradeInfo> {
        return httpClient.queryPrivateApi(Operation.QUERY_TRADES, QueryTradesFilter.toPayloadObject(filter))
            .let { queryTradesResponseAdapter.fromJson(it.bodyString())!! }
            .also { verifyApiResponseErrors(it, log) }
            .toDomainObject()
    }

    override fun getTradeVolume(pairs: List<String>): TradeVolumeInfo {
        val request = mapOf(
            "pair" to pairs.joinToString(separator = ","),
            "fee-info" to true
        )
        return httpClient.queryPrivateApi(Operation.GET_TRADE_VOLUME, request)
            .let { tradeVolumeResponseAdapter.fromJson(it.bodyString())!! }
            .also { verifyApiResponseErrors(it, log) }
            .result
    }

    override suspend fun addOrder(order: AddOrder): AddOrderStatusMessage {
        // currently this method will require to be already subscribed to websocket
        if (null == websocket) {
            throw NotSubscribedException(id)
        }
        // find already obtained token
        val token = subscribedRequests.first().subscription.token!!
        // convert to request
        val req = AddOrder.toRequest(order = order, token = token, reqid = timestampSupplier.invoke())

        val reqString = AddOrderRequest.jsonAdapter.toJson(req)
        log.info("Add order: {} ", reqString)
        websocket?.send(WsMessage(reqString))

        // register in response handler
        val channel = addOrderStatusHandlerImpl.registerRequest(req)

        // wait for channel to receive response (suspend coroutine)
        val response = channel.receive()

        // type-safe check
        if (response is AddOrderStatusMessage) {
            return response
        } else {
            throw RuntimeException("[KRAKEN-$id]: AddOrder response class does not match, request-reply is not completed correctly")
        }
    }

    override suspend fun cancelOrder(order: CancelOrder): CancelOrderStatusMessage {
        // currently this method will require to be already subscribed to websocket
        if (null == websocket) {
            throw NotSubscribedException(id)
        }
        // find already obtained token
        val token = subscribedRequests.first().subscription.token!!
        // convert to request
        val req = CancelOrder.toRequest(order = order, token = token, reqid = timestampSupplier.invoke())

        val reqString = CancelOrderRequest.jsonAdapter.toJson(req)
        log.info("Cancel order, or orders: {} ", reqString)
        websocket?.send(WsMessage(reqString))

        // register in response handler
        val channel = cancelOrderStatusHandlerImpl.registerRequest(req)

        // wait for channel to receive response (suspend coroutine)
        val response = channel.receive()

        // type-safe check
        if (response is CancelOrderStatusMessage) {
            return response
        } else {
            throw RuntimeException("[KRAKEN-$id]: CancelOrder response class does not match, request-reply is not completed correctly")
        }
    }

    override fun subscribe(
        channels: Set<Channel>,
        handlers: Collection<MessageHandler<*>>
    ) {

        // close if active
        websocket?.close()

        // handlers
        if (handlers.isEmpty()) log.warn("Empty list of handlers, probably not configured properly!")
        this.handlers = this.handlers.plus(handlers)

        // subscription list
        subscribedRequests = channels.map {
            SubscribeRequest(
                event = KrakenPublicClientImpl.SUBSCRIBE,
                subscription = SubscribeRequest.Subscription(name = it.channelName))
        }

        log.info("Will connect websocket, with subscriptions: $subscribedRequests")
        connect(subscribedRequests)
    }

    private fun connect(subscribed: List<SubscribeRequest>) {
        // verify it was some time since last reconnect
        val lastReconnectInstant = lastReconnectAttempt.get()
        val now = Instant.now()
        if (now.isBefore(lastReconnectInstant.plus(reconnectAttemptInterval))) {
            log.trace("Will wait, lastReconnect occurred $lastReconnectInstant")
            return
        }

        // reset heartbeat
        heartbeatHandler.reset()

        // ok we are some time after the last attempt, lets make sure we are alone
        if (lastReconnectAttempt.compareAndSet(lastReconnectInstant, now)) {
            log.info("Will reconnect PRIVATE websocket...")
            // fetch token
            val token = getWebSocketToken()

            // add token to each subscription
            subscribed.forEach {
                it.subscription.token = token.token
            }
            log.trace("Fetched token, create websocket...")

            // connect websocket
            websocket = WebsocketClient.nonBlocking(uri = Uri.of(webSocketUrl)) { ws ->
                ws.onMessage { messageHandler(it.bodyString()) }
                ws.onError { log.error("Websocket error: ", it) }
                ws.onClose {
                    log.warn("Websocket closed: ${it.description}. Reconnect will be triggered by next PING req...")
                }
                /* send subscriptions */
                subscribed.forEach {
                    val reqStr = SubscribeRequest.jsonAdapter.toJson(it)
                    log.info("Sending subscribe: $reqStr")
                    ws.send(WsMessage(reqStr))
                }

                /* connection checker */
                connectionCheck?.cancel() // cancel previous
                connectionCheck = scheduleConnectionCheck(ws)
            }
        } else {
            log.info("Unable to set lastReconnectAttempt time, probably race conditions during reconnect of Private Websocket.")
        }
    }

    private fun messageHandler(payload: String) {
        log.trace("Received message : {}", payload)

        when (val channel = resolveChannel(payload)) {
            Channel.SystemStatus -> handlers.filterIsInstance<SystemStatusHandler>().forEach { it.handleMessage(SystemStatusMessage.toKotlinObject(payload)) }
            Channel.OwnTrades -> handlers.filterIsInstance<OwnTradesHandler>().forEach { it.handleMessage(OwnTradesMessage.toKotlinObject(payload)) }
            Channel.OpenOrders -> handlers.filterIsInstance<OpenOrdersHandler>().forEach { it.handleMessage(OpenOrdersMessage.toKotlinObject(payload)) }
            Channel.Pong -> handlers.filterIsInstance<PongHandler>().forEach { it.handleMessage(PingPongMessage.toKotlinObject(payload)) }
            Channel.Heartbeat -> handlers.filterIsInstance<HeartbeatHandler>().forEach { it.handleMessage(HeartbeatMessage.instance) }
            Channel.SubscriptionStatus -> handlers.filterIsInstance<SubscriptionStatusHandler>().forEach { it.handleMessage(SubscriptionStatusMessage.toKotlinObject(payload)) }
            Channel.AddOrderStatus -> handlers.filterIsInstance<AddOrderStatusHandler>().forEach { it.handleMessage(AddOrderStatusMessage.toKotlinObject(payload)) }
            Channel.CancelOrderStatus -> handlers.filterIsInstance<CancelOrderStatusHandler>().forEach { it.handleMessage(CancelOrderStatusMessage.toKotlinObject(payload)) }
            else -> log.trace("Undefined handler for $channel") // ok, just log
        }
    }

    private fun scheduleConnectionCheck(ws: Websocket) = schedule(interval = connectionCheckInterval) {
        // send ping with timestamp
        val stamp = timestampSupplier.invoke()
        log.trace("Will send ping request with stamp {}", stamp)
        val pingReq = PingPongMessage.jsonAdapter.toJson(PingPongMessage(event = PingPongMessage.PING, reqid = stamp))

        // check heartbeat interval
        val lastHeartbeat = heartbeatHandler.lastReceived
        if (Instant.now().isAfter(lastHeartbeat.plus(heartbeatTimeout))) {
            log.info("Heartbeat was not received in the last $heartbeatTimeout , will reconnect websocket!")
            reconnectWebsocket(ws, "[HEARTBEAT] None heartbeat, will reconnect.")
        }

        // send with reconnect check
        if (sendWsReqWithReconnectCheck(ws, pingReq) { reconnectWebsocket(ws, "[PING] Unable to send ping request") }) {
            log.trace("Request sent, will wait for response")

            // ok, request sent, now wait for response (pong)
            scheduleOnce(delay = connectionCheckTimeout) {
                val receivedPong = pongHandlerImpl.receivedPong
                log.trace("Received pong reqId: {} expected : {}", receivedPong?.reqid, stamp)

                // verify in response there is expected timestamp
                if (stamp != receivedPong?.reqid) {
                    reconnectWebsocket(ws, "[PING] No response for ping, expected stamp $stamp received ${receivedPong?.reqid}")
                }
            }
        }
    }

    private fun reconnectWebsocket(ws: Websocket, msg: String) {
        log.warn("Reconnect private websocket is needed, due to: $msg")
        ws.close()
        connect(subscribedRequests)
    }

    /**
     * Private http client to query Kraken API.
     */
    private class PrivateHttpClient(val log: Logger, val apiHost: String, val apiKey: String, val privateKey: String, val rateLimit: KrakenPrivateClient.RateLimit, val krakenMetricsConsumer: KrakenPrivateClient.KrakenMetricsConsumer) {
        private val http by lazy {
            OkHttp()
        }
        // call cache
        private val callCache = CacheBuilder.newBuilder()
                .expireAfterWrite(rateLimit.countMaximum * rateLimit.decreaseByOneInSeconds, TimeUnit.SECONDS)
                .build<String, CachedCall>()

        /**
         * Nonce string, should be always increasing number.
         */
        fun generateNonce() : String = "" + System.currentTimeMillis() + "000" // for example 1600885197478000, improve if it should collide

        /**
         * Signature is computed using this formula:
         * hmac_sha512(path + sha256(nonce + postdata), b64decode(secret))
         *
         * and the result is converted in a base64 string.
         */
        fun createSignature(urlPath: String, nonce: String, postData: String, secret: String): String {
            // sha256(nonce + postdata)
            val shaSum = Hashing.sha256().hashBytes(nonce.toByteArray().plus(postData.toByteArray())).asBytes()
            // base64 decode secret key
            val hmacKey = BaseEncoding.base64().decode(secret)
            // path + sha256(...)
            val hmacPayload = urlPath.toByteArray().plus(shaSum)
            return BaseEncoding.base64().encode(Hashing.hmacSha512(hmacKey).hashBytes(hmacPayload).asBytes())
        }

        fun queryPrivateApi(operation: Operation, payload: Any = emptyMap<String, String>()): Response {
            // stats
            krakenMetricsConsumer.increaseQueryCount()
            val currentWeight = callCache.asMap().values.sumBy { x -> x.weight.toInt() }
            krakenMetricsConsumer.setCurrentRateLimit(currentWeight)

            // possible race conditions, no locks
            if (currentWeight >= rateLimit.countMaximum) {
                // we are at maximum and should not continue
                log.debug("Soft rate limit exceeded, with this history of calls: {}",
                        callCache.asMap().values.map { "${it.timestamp} : ${it.api}" })
                throw RateLimitExceeded()
            }

            // put to cache, will do request
            callCache.put(UUID.randomUUID().toString(), CachedCall(operation.name, LocalDateTime.now(), operation.weight))

            // serialize object fields to map
            val adapter = moshi.adapter(Any::class.java)
            val jsonStructure = adapter.toJsonValue(payload)
            val params = jsonStructure as MutableMap<String, Any>

            // nonce and uriPath
            val nonce = generateNonce()
            val uriPath = operation.uriPath

            // nonce should be first, create POST payload nonce=1601041173874000&orderId=42
            val paramsString = "nonce=$nonce&" + params.toUrlQueryString()

            // create signature
            val signature = createSignature(uriPath, nonce, paramsString, privateKey)

            log.trace("Will perform request on {}, with paramsString {} and signature {}", apiHost + uriPath, paramsString, signature)
            return http(Request(Method.POST, apiHost + uriPath)
                    .body(paramsString)
                    .header("User-Agent", "Kraken Javascript API Client")
                    .header(API_KEY_HEADER, apiKey)
                    .header(API_SIGNATURE_HEADER, signature)
                    .also { log.trace("Request : {}", it) })
                    .also { log.trace("Response : {}", it) }
                    .also { verifyHttpStatusResponse(it, log) }
        }
    }

    /**
     * Structure to store api call.
     */
    private data class CachedCall(val api: String, val timestamp: LocalDateTime, val weight: Long = 1)

    /**
     * Pong handler for connection check.
     */
    class PongHandlerImpl : PongHandler {
        var receivedPong: PingPongMessage? = null

        override fun handleMessage(message: PingPongMessage) {
            receivedPong = message
        }
    }

    /**
     * AddOrderStatus message handler.
     */
    class AddOrderStatusHandlerImpl(requestReplyTimeout: Duration) :
        BaseRequestReplyMessageHandler<AddOrderRequest, AddOrderStatusMessage>(requestReplyTimeout = requestReplyTimeout.seconds, log = log, expiredMessage = expiredMessage()),
        AddOrderStatusHandler {

        override fun handleMessage(message: AddOrderStatusMessage) {
            handleMessageInternal(message)
        }

        companion object {
            val log: Logger = LoggerFactory.getLogger(AddOrderStatusHandlerImpl::class.java)
            fun expiredMessage() : (reqid: Long, status: OrderStatus, errorMessage: String) -> AddOrderStatusMessage =
                { reqid, status, errorMessage -> AddOrderStatusMessage(reqid = reqid, status = status, errorMessage = errorMessage, descr = null, txid = null) }
        }
    }

    /**
     * CancelOrderStatus message handler.
     */
    class CancelOrderStatusHandlerImpl(requestReplyTimeout: Duration) :
        BaseRequestReplyMessageHandler<CancelOrderRequest, CancelOrderStatusMessage>(requestReplyTimeout = requestReplyTimeout.seconds, log = log, expiredMessage = expiredMessage()),
        CancelOrderStatusHandler {

        override fun handleMessage(message: CancelOrderStatusMessage) {
            handleMessageInternal(message)
        }

        companion object {
            val log: Logger = LoggerFactory.getLogger(CancelOrderStatusHandlerImpl::class.java)
            fun expiredMessage() : (reqid: Long, status: OrderStatus, errorMessage: String) -> CancelOrderStatusMessage =
                { reqid, status, errorMessage -> CancelOrderStatusMessage(reqid = reqid, status = status, errorMessage = errorMessage) }
        }
    }

    object NoOpKrakenMetricsConsumer : KrakenPrivateClient.KrakenMetricsConsumer {
        override fun increaseQueryCount() {
            // do nothing
        }

        override fun setCurrentRateLimit(rateLimit: Int) {
            // do nothing
        }
    }

    companion object {
        const val API_KEY_HEADER = "API-Key"
        const val API_SIGNATURE_HEADER = "API-Sign"
        const val API_PREFIX = "/0/private/"

        enum class Operation(val uriPath: String, val weight: Long = 1) {
            ACCOUNT_BALANCE("${API_PREFIX}Balance"),
            TRADE_BALANCE("${API_PREFIX}TradeBalance"),
            GET_WEBSOCKET_TOKEN("${API_PREFIX}GetWebSocketsToken"),
            CLOSED_ORDERS("${API_PREFIX}ClosedOrders"),
            QUERY_ORDERS("${API_PREFIX}QueryOrders"),
            QUERY_TRADES("${API_PREFIX}QueryTrades"),
            TRADES_HISTORY(uriPath = "${API_PREFIX}TradesHistory", weight = 2L),
            GET_TRADE_VOLUME("${API_PREFIX}TradeVolume")
        }
    }
}

package kraken.api

import kraken.api.http.data.Asset
import kraken.api.http.data.AssetPair
import kraken.api.http.data.OrderBook
import kraken.api.ws.Channel
import kraken.api.ws.MessageHandler

/**
 * Public endpoints - https://www.kraken.com/features/api#public-market-data
 */
interface KrakenPublicClient {

    /**
     * Synchronous messages
     */
    fun getAssets(): List<Asset>

    fun getAssetPairs(): List<AssetPair>

    fun getOrderBook(assetCode: String): OrderBook

    /**
     * Subscribe to asynchronous messages
     */
    fun subscribe(
        channels: Set<Channel>,
        handlers: Collection<MessageHandler<*>> = emptyList(),
        assetPairs: Collection<String> = emptyList()
    )
}

package kraken.api.infra

import java.math.BigDecimal

/**
 * compare two bigdecimal objects without considering scales, return true if equals.
 */
fun BigDecimal.comparedEquals(another : BigDecimal): Boolean = this.compareTo(another) == 0

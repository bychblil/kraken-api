package kraken.api.infra

import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.Duration

val log: Logger = LoggerFactory.getLogger("kraken.api.infra")

fun scheduleOnce(delay: Duration, run: () -> Any): Job = GlobalScope.launch {
    try {
        delay(delay.toMillis())
        run()
    } catch (t: Throwable) {
        if (t !is CancellationException) {
            log.error("Exception in scheduled function", t)
        }
    }
}

fun schedule(interval: Duration, run: () -> Any): Job = GlobalScope.launch {
    while (isActive) {
        try {
            delay(interval.toMillis())
            run()
        } catch (t: Throwable) {
            if (t !is CancellationException) {
                log.error("Exception in scheduled function", t)
            }
        }
    }
}

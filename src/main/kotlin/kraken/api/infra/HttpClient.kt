package kraken.api.infra

import kraken.api.http.data.response.ApiResponse
import kraken.api.http.exception.HttpStatusException
import kraken.api.http.exception.ResponseBusinessException
import org.http4k.core.Response
import org.slf4j.Logger

fun verifyHttpStatusResponse(response: Response, log: Logger) {
    if (!response.status.successful) {
        log.warn("Received not successful response: {}", response)
        throw HttpStatusException(response.status)
    }
}

fun verifyApiResponseErrors(response: ApiResponse, log: Logger) {
    if (response.error.isNotEmpty()) {
        log.warn("Received error from API : {}", response)
        throw ResponseBusinessException(response.error)
    }
}

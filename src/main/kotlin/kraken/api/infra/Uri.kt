package kraken.api.infra

import java.net.URLEncoder

fun Map<String, Any>.toUrlQueryString() =
        this.map { (k, v) -> "${URLEncoder.encode(k, "UTF-8")}=${URLEncoder.encode("" + v, "UTF-8")}" }
                .joinToString("&")

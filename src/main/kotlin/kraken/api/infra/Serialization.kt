package kraken.api.infra

import com.squareup.moshi.FromJson
import com.squareup.moshi.Moshi
import com.squareup.moshi.ToJson
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import java.math.BigDecimal
import java.time.Instant

object BigDecimalAdapter {
    @FromJson
    fun fromJson(string: String) = BigDecimal(string)
    @ToJson
    fun toJson(value: BigDecimal) = value.toString()
}

// instant to timestamp adapter
object CustomInstantAdapter {
    @FromJson fun fromJson(string: String) = instantOfEpochSecondOrNull(string.toBigDecimal().toLong())
    @ToJson fun toJson(value: Instant) = "${value.epochSecond}.0" // only seconds precision
}

fun instantOfEpochSecondOrNull(time : Long) : Instant? {
    // 0L is in API "undefined"
    if (time > 0) {
        return Instant.ofEpochSecond(time)
    } else {
        return null
    }
}

val moshi: Moshi = Moshi.Builder()
        .add(BigDecimalAdapter)
        .add(CustomInstantAdapter)
        .add(KotlinJsonAdapterFactory())
        .build()

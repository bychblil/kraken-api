package kraken.api

import kraken.api.http.data.response.FuturesFeeScheduleResponse
import kraken.api.http.data.response.InstrumentsResponse

/** Public endpoints - https://support.kraken.com/hc/en-us/sections/360003562331-REST-API-Public */
interface KrakenFuturesPublicClient {
    fun feeSchedule(): FuturesFeeScheduleResponse
    fun instruments(): InstrumentsResponse
}

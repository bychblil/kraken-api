package kraken.api.http

import com.google.common.cache.CacheBuilder
import com.google.common.hash.Hashing
import com.google.common.io.BaseEncoding
import kraken.api.http.exception.RateLimitExceeded
import kraken.api.infra.moshi
import kraken.api.infra.toUrlQueryString
import kraken.api.infra.verifyHttpStatusResponse
import org.http4k.client.OkHttp
import org.http4k.core.Method
import org.http4k.core.Method.GET
import org.http4k.core.Method.POST
import org.http4k.core.Request
import org.http4k.core.Response
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.LocalDateTime
import java.util.UUID
import java.util.concurrent.TimeUnit

class FuturesPrivateHttpClient(
    val log: Logger = LoggerFactory.getLogger(FuturesPrivateHttpClient::class.java),
    val apiHost: String = "https://futures.kraken.com/derivatives",
    val apiKey: String,
    val privateKey: String
) {
    private val http by lazy { OkHttp() }

    private val callCache = CacheBuilder.newBuilder()
        .expireAfterWrite(10, TimeUnit.SECONDS)
        .build<String, CachedCall>()

    fun openPositions(): String = queryPrivateApi(Operation.OPEN_POSITIONS).bodyString()

    fun accounts(): String = queryPrivateApi(Operation.ACCOUNTS).bodyString()

    fun openOrders(): String = queryPrivateApi(Operation.OPEN_ORDERS).bodyString()

    fun tradeVolume(): String = queryPrivateApi(Operation.TRADE_VOLUME).bodyString()

    /** Nonce string, should be always increasing number. */
    private fun generateNonce(): String = "" + System.currentTimeMillis() + "000" // for example 1600885197478000, improve if it should collide

    /**
     * Signature is computed using this formula:
     * hmac_sha512(sha256(postdata + nonce + path), b64decode(secret))
     *
     * and the result is converted in a base64 string.
     */
    private fun createSignature(urlPath: String, nonce: String, postData: String, secret: String): String {
        // sha256(postdata + nonce + urlPath)
        val shaSum = Hashing.sha256().hashBytes(postData.toByteArray() + nonce.toByteArray() + urlPath.toByteArray()).asBytes()
        // base64 decode secret key
        val hmacKey = BaseEncoding.base64().decode(secret)
        return BaseEncoding.base64().encode(Hashing.hmacSha512(hmacKey).hashBytes(shaSum).asBytes())
    }

    private fun queryPrivateApi(operation: Operation, payload: Any = emptyMap<String, String>()): Response {
        // possible race conditions, no locks
        if (callCache.asMap().values.sumBy { x -> x.weight.toInt() } >= 500) {
            // we are at maximum and should not continue
            log.debug("Soft rate limit exceeded, with this history of calls: {}",
                callCache.asMap().values.map { "${it.timestamp} : ${it.api}" })
            throw RateLimitExceeded()
        }

        // put to cache, will do request
        callCache.put(
            UUID.randomUUID().toString(),
            CachedCall(operation.name, LocalDateTime.now(), operation.weight)
        )

        // serialize object fields to map
        val adapter = moshi.adapter(Any::class.java)
        val jsonStructure = adapter.toJsonValue(payload)
        val params = (jsonStructure as MutableMap<String, Any>).toUrlQueryString()

        val nonce = generateNonce()
        val uriPath = operation.uriPath
        val signature = createSignature(uriPath, nonce, params, privateKey)

        log.trace("Will perform request on {}, with paramsString {} and signature {}", apiHost + uriPath, params, signature)
        return http(
            Request(operation.httpMethod, "$apiHost$uriPath?$params")
            .body(params)
            .header("User-Agent", "Kraken API Client")
            .header(API_KEY_HEADER, apiKey)
            .header(NONCE_HEADER, nonce)
            .header(API_AUTHENT_HEADER, signature)
            .also { log.trace("Request : {}", it) })
            .also { log.trace("Response : {}", it) }
            .also { verifyHttpStatusResponse(it, log) }
    }

    companion object {
        const val API_KEY_HEADER = "APIKey"
        const val NONCE_HEADER = "Nonce"
        const val API_AUTHENT_HEADER = "Authent"
        const val API_PREFIX = "/api/v3"

        enum class Operation(val httpMethod: Method, val uriPath: String, val weight: Long = 1) {
            SEND_ORDER(POST, "$API_PREFIX/sendorder", weight = 10),
            EDIT_ORDER(POST, "$API_PREFIX/editorder", weight = 10),
            CANCEL_ORDER(POST, "$API_PREFIX/cancelorder", weight = 10),
            ACCOUNTS(GET, "$API_PREFIX/accounts", weight = 2),
            OPEN_POSITIONS(GET, "$API_PREFIX/openpositions", weight = 2),
            FILLS(GET, "$API_PREFIX/fills", weight = 2),
            FILLS_SPECIFIED_LAST_FILL_TIME(GET, "$API_PREFIX/fills", weight = 25),
            OPEN_ORDERS(GET, "$API_PREFIX/openorders", weight = 2),
            TRADE_VOLUME(GET, "$API_PREFIX/feeschedules/volumes")
        }
    }
}

/** Structure to store api call */
private data class CachedCall(val api: String, val timestamp: LocalDateTime, val weight: Long = 1)

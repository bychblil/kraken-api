package kraken.api.http.data

import java.math.BigDecimal

/**
 * Trade balance domain object.
 *
 * eb = equivalent balance (combined balance of all currencies)
 * tb = trade balance (combined balance of all equity currencies)
 * m = margin amount of open positions
 * n = unrealized net profit/loss of open positions
 * c = cost basis of open positions
 * v = current floating valuation of open positions
 * e = equity = trade balance + unrealized net profit/loss
 * mf = free margin = equity - initial margin (maximum margin available to open new positions)
 * ml = margin level = (equity / initial margin) * 100
 */
class TradeBalance (
    val eb: BigDecimal,
    val tb: BigDecimal,
    val m: BigDecimal,
    val n: BigDecimal,
    val c: BigDecimal,
    val v: BigDecimal,
    val e: BigDecimal,
    val mf: BigDecimal,
    val ml: BigDecimal?
)

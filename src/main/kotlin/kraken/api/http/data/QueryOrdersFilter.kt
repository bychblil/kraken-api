package kraken.api.http.data

data class QueryOrdersFilter(
    val trades: Boolean = false, // include trades or not
    val txIds: Set<String> // list of transaction ids
) {
    class Payload(
        val trades: Boolean,
        val txid: String
    )

    companion object {

        fun toPayloadObject(queryOrdersFilter: QueryOrdersFilter) : QueryOrdersFilter.Payload =
            Payload(trades = queryOrdersFilter.trades, txid = queryOrdersFilter.txIds.joinToString(separator = ","))
    }
}

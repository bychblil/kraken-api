package kraken.api.http.data

import java.math.BigDecimal

/**
 * Balance domain object.
 */
data class Balance(val assetId: String, val balance: BigDecimal)

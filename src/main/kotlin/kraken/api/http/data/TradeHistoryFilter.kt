package kraken.api.http.data

import kraken.api.infra.CustomInstantAdapter
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.Instant

/**
 * Filter for trade history.
 */
data class TradeHistoryFilter(
    val type: TypeFilter? = null,
    val startTradeId: String? = null, // exclusive
    val startTime: Instant? = null, // exclusive, use seconds precision
    val endTradeId: String? = null, // inclusive
    val endTime: Instant? = null, // inclusive, use seconds precision
    val includeTrades: Boolean = true,
    val offset: Long? = null
) {
    data class Payload(
        val type: String? = null,
        val start: String? = null, // exclusive
        val end: String? = null, // inclusive
        val trades: Boolean = true,
        val ofs: Long? = null // result offset
    )

    enum class TypeFilter(val code: String) {
        ALL("all"), // default
        ANY_POSITION("any position"),
        CLOSED_POSITION("closed position"),
        CLOSING_POSITION("closing position"),
        NO_POSITION("no position")
    }

    companion object {
        val log: Logger = LoggerFactory.getLogger(TradeHistoryFilter::class.java)

        fun toPayloadObject(filter: TradeHistoryFilter) : Payload {
            if (null != filter.startTime && null != filter.startTradeId) {
                log.info("Both startTime and startTradeId were set, startTime will have priority!")
            }
            if (null != filter.endTime && null != filter.endTradeId) {
                log.info("Both endTime and endTradeId were set, endTime will have priority!")
            }
            return Payload(type = filter.type?.code, start = serializeInstant(filter.startTime) ?: filter.startTradeId,
                end = serializeInstant(filter.endTime) ?: filter.endTradeId, trades = filter.includeTrades,
                ofs = filter.offset
            )
        }

        private fun serializeInstant(instant: Instant?): String? {
            if (null == instant) {
                return null
            }
            return CustomInstantAdapter.toJson(instant)
        }
    }
}

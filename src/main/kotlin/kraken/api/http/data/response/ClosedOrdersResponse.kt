package kraken.api.http.data.response

import kraken.api.domain.OrderDescription
import java.math.BigDecimal
import java.time.Instant

class ClosedOrdersResponse (error : List<String>, val result: Payload) : ApiResponse(error) {

    class Payload(val closed: Map<String, ClosedOrder>, val count: Long)

    class ClosedOrder(
        val refid: String?,
        val userref: String?,
        val status: String,
        val reason: String?,
        val opentm: Instant,
        val closetm: Instant,
        val starttm: Instant?,
        val expiretm: Instant?,
        val descr: OrderDescription,
        val vol: BigDecimal,
        val vol_exec: BigDecimal,
        val cost: BigDecimal,
        val fee: BigDecimal,
        val price: BigDecimal,
        val stopprice: BigDecimal,
        val limitprice: BigDecimal,
        val misc: String,
        val oflags: String
    )
}

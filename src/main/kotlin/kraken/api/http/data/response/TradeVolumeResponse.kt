package kraken.api.http.data.response

import kraken.api.domain.TradeVolumeInfo

class TradeVolumeResponse(error: List<String>, val result: TradeVolumeInfo) : ApiResponse(error)

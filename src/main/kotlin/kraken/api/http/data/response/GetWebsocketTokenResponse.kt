package kraken.api.http.data.response

import kraken.api.http.data.WebSocketToken

class GetWebsocketTokenResponse(error: List<String>, val result: WebSocketToken) : ApiResponse(error)

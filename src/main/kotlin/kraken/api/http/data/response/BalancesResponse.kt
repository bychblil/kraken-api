package kraken.api.http.data.response

import java.math.BigDecimal

class BalancesResponse (error : List<String>, val result: Map<String, BigDecimal>?) : ApiResponse(error)

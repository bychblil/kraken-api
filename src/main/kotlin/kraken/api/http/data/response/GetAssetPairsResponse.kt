package kraken.api.http.data.response

import kraken.api.http.data.AssetPair
import java.math.BigDecimal

class GetAssetPairsResponse (error : List<String>, val result: Map<String, AssetPairInResponse>?) : ApiResponse(error) {

    data class AssetPairInResponse (
        val altname: String,
        val wsname: String?,
        val base: String,
        val quote: String,
        val pair_decimals: Long,
        val lot_decimals: Long,
        val fee_volume_currency: String,
        val fees: Array<Array<BigDecimal>>,
        val fees_maker: Array<Array<BigDecimal>>?,
        val ordermin: BigDecimal?
    )

    companion object fun mapToAssetPairList(resp : GetAssetPairsResponse) : List<AssetPair> =
        resp.result?.map { (k, v) ->
            AssetPair(k, v.altname, v.wsname, v.base, v.quote,
                    v.pair_decimals, v.lot_decimals, v.fee_volume_currency,
                    v.fees.map { arr -> AssetPair.Fee(arr[0].longValueExact(), arr[1]) },
                    v.fees_maker?.map { arr -> AssetPair.Fee(arr[0].longValueExact(), arr[1]) },
                    v.ordermin)
        } ?: emptyList()
}

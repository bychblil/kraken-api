package kraken.api.http.data.response

import java.math.BigDecimal

data class FuturesFeeScheduleResponse(val feeSchedules: List<FeeSchedule>) {

    data class FeeSchedule(
        val name: String,
        val tiers: List<FeeTier>
    )

    data class FeeTier(
        val makerFee: BigDecimal,
        val takerFee: BigDecimal,
        val usdVolume: BigDecimal
    )
}

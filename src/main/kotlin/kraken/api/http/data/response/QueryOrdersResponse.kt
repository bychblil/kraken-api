package kraken.api.http.data.response

import kraken.api.domain.OrderInfo

class QueryOrdersResponse (error : List<String>, val result: Map<String, OrderInfo>?) : ApiResponse(error)

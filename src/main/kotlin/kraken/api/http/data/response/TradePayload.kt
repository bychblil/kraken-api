package kraken.api.http.data.response

import java.math.BigDecimal
import java.time.Instant

data class TradePayload(
    val ordertxid: String,
    val postxid: String?,
    val pair: String,
    val time: Instant,
    val type: String,
    val ordertype: String,
    val price: BigDecimal,
    val cost: BigDecimal,
    val fee: BigDecimal,
    val vol: BigDecimal,
    val margin: BigDecimal?,
    val misc: String?
)

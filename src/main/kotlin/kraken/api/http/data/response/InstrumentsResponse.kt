package kraken.api.http.data.response

import java.math.BigDecimal
import java.math.BigDecimal.ZERO

data class InstrumentsResponse(
    val instruments: List<Instrument>
) {
    data class Instrument(
        val symbol: String,
        val type: String,
        val underlying: String?,
        val tickSize: BigDecimal = ZERO, // default values for non tradeable instruments
        val contractSize: BigDecimal = ZERO, // default values for non tradeable instruments
        val tradeable: Boolean,
        val fundingRateCoefficient: BigDecimal?,
        val maxRelativeFundingRate: BigDecimal?,
        val marginLevels: List<MarginLevel> = emptyList() // default values for non tradeable instruments
    )

    data class MarginLevel(
        val contracts: Long,
        val initialMargin: BigDecimal,
        val maintenanceMargin: BigDecimal
    )
}

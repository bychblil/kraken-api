package kraken.api.http.data.response

class GetAssetsResponse(error: List<String>, val result: Map<String, AssetInResponse>?) : ApiResponse(error) {

    data class AssetInResponse(
        val altname: String,
        val decimals: Long,
        val display_decimals: Long
    )
}

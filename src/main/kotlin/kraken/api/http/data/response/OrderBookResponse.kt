package kraken.api.http.data.response

import kraken.api.http.data.OrderBook
import java.math.BigDecimal
import java.time.Instant

class OrderBookResponse(error: List<String>, val result: Map<String, KrakenOrderBook>?) : ApiResponse(error) {

    data class KrakenOrderBook(
        val asks: List<Array<String>> = emptyList(),
        val bids: List<Array<String>> = emptyList()
    )

    fun mapToOrderBook(): OrderBook = if (result.isNullOrEmpty()) OrderBook() else OrderBook(
        asks = result[result.keys.first()]!!.asks.map { it.toBookEntry() },
        bids = result[result.keys.first()]!!.bids.map { it.toBookEntry() }
    )

    private fun Array<String>.toBookEntry() = OrderBook.BookEntry(
        BigDecimal(this[0]),
        BigDecimal(this[1]),
        Instant.ofEpochSecond(this[2].toLong())
    )
}

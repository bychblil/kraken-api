package kraken.api.http.data.response

import kraken.api.http.data.TradeBalance

class TradeBalanceResponse (error : List<String>, val result: TradeBalance) : ApiResponse(error)

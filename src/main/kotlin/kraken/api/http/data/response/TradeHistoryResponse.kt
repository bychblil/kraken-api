package kraken.api.http.data.response

import kraken.api.domain.KrakenOrderType
import kraken.api.domain.KrakenTradeType
import kraken.api.domain.TradeInfo

class TradeHistoryResponse(error: List<String>, val result: TradeWrapper?) : ApiResponse(error) {

    data class TradeWrapper(
        val trades: Map<String, TradePayload>
    )

    fun toDomainObject(): Map<String, TradeInfo> {
        if (null == result || result.trades.isNullOrEmpty()) {
            return emptyMap()
        }

        return result.trades.mapValues {
            TradeInfo(
                tradeId = it.key, orderTxId = it.value.ordertxid, posTxId = it.value.postxid, assetPair = it.value.pair,
                time = it.value.time, type = KrakenTradeType.from(it.value.type), orderType = KrakenOrderType.from(it.value.ordertype),
                price = it.value.price, cost = it.value.cost, fee = it.value.fee, volume = it.value.vol,
                margin = it.value.margin, misc = it.value.misc
            )
        }
    }
}

package kraken.api.http.data.response

/**
 * All API responses share the same structure
 */
open class ApiResponse (val error: List<String>)

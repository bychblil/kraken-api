package kraken.api.http.data

import java.math.BigDecimal
import java.time.Instant

data class OrderBook(
    val asks: List<BookEntry> = emptyList(),
    val bids: List<BookEntry> = emptyList()
) {
    data class BookEntry(
        val price: BigDecimal,
        val volume: BigDecimal,
        val timestamp: Instant
    )
}

package kraken.api.http.data

data class Asset (
    val id: String,
    val altname: String,
    val decimals: Long,
    val display_decimals: Long
)

package kraken.api.http.data

import java.math.BigDecimal

data class AssetPair(
    val id: String,
    val altname: String,
    val wsname: String?,
    val base: String,
    val quote: String,
    val pair_decimals: Long,
    val lot_decimals: Long,
    val fee_volume_currency: String,
    val fees_trader: List<Fee>,
    val fees_maker: List<Fee>?,
    val ordermin: BigDecimal?
) {

    data class Fee (val volume: Long, val fee: BigDecimal)
}

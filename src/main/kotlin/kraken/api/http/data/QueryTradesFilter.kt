package kraken.api.http.data

data class QueryTradesFilter(
    val txIds: Set<String> // list of transaction ids
) {
    class Payload(
        val txid: String
    )

    companion object {

        fun toPayloadObject(queryTradesFilter: QueryTradesFilter): Payload =
            Payload(txid = queryTradesFilter.txIds.joinToString(separator = ","))
    }
}

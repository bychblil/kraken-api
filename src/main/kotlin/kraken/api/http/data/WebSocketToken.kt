package kraken.api.http.data

data class WebSocketToken(val token: String, val expires: Long)

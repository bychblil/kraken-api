package kraken.api.http.data

/**
 * Filter to search for closed orders
 */
class ClosedOrdersFilter(
    val userref: String? = null,
    val ofs: Long? = null
)

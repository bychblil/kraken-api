package kraken.api.http.exception

import org.http4k.core.Status
import java.lang.RuntimeException

class HttpStatusException(status: Status) : RuntimeException(status.toString())

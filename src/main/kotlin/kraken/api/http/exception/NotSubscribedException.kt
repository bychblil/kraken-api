package kraken.api.http.exception

/**
 * Exception to be thrown when private client is used wrongly
 */
class NotSubscribedException(id: String) : RuntimeException("$id - Not subscribed to websocket, subscribe first.")

package kraken.api.http.exception

/**
 * Business exception from Kraken API - directly in body of response.
 */
class ResponseBusinessException(errors: List<String>) : RuntimeException(errors.joinToString())

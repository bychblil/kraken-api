package kraken.api.http.exception

/**
 * Exception to be thrown when rate limit would be exceeded, so request was cancelled.
 */
class RateLimitExceeded : RuntimeException("Soft rate limit on API exceeded")

package kraken.api.domain

import com.squareup.moshi.Json

enum class KrakenOrderStatus(val code: String) {
    @Json(name = "pending") PENDING("pending"),
    @Json(name = "open") OPEN("open"),
    @Json(name = "closed") CLOSED("closed"),
    @Json(name = "canceled") CANCELED("canceled"),
    @Json(name = "expired") EXPIRED("expired");

    companion object {
        fun from(statusCode: String) =
            values().find { it.code == statusCode } ?: throw IllegalArgumentException("Unknown status for open order $statusCode")
    }
}

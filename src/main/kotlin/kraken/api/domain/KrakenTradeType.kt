package kraken.api.domain

import com.squareup.moshi.Json

enum class KrakenTradeType(val code: String) {
    @Json(name = "buy") BUY("buy"),
    @Json(name = "sell") SELL("sell")
    ;

    companion object {
        fun from(code: String) =
            values().find { it.code == code } ?: throw IllegalArgumentException("Unknown trade type  $code")
    }
}

package kraken.api.domain

import java.math.BigDecimal

data class TradeVolumeInfo(
    val currency: String,
    val volume: BigDecimal,
    val fees: Map<String, Fee>,
    val fees_maker: Map<String, Fee>
) {
    data class Fee(
        val fee: BigDecimal,
        val minfee: BigDecimal,
        val maxfee: BigDecimal,
        val nextfee: BigDecimal,
        val nextvolume: BigDecimal,
        val tiervolume: BigDecimal
    )
}

package kraken.api.domain

import java.math.BigDecimal

data class OrderDescription(
    val pair: String,
    val type: KrakenTradeType,
    val ordertype: KrakenOrderType,
    val price: BigDecimal,
    val price2: BigDecimal,
    val leverage: String,
    val order: String, // description
    val close: String?
)

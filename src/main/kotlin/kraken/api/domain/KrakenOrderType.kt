package kraken.api.domain

import com.squareup.moshi.Json

enum class KrakenOrderType(val code: String) {
    @Json(name = "market") MARKET("market"),
    @Json(name = "limit") LIMIT("limit"),
    @Json(name = "stop-loss") STOP_LOSS("stop-loss"),
    @Json(name = "take-profit") TAKE_PROFIT("take-profit"),
    @Json(name = "stop-loss-limit") STOP_LOSS_LIMIT("stop-loss-limit"),
    @Json(name = "take-profit-limit") TAKE_PROFIT_LIMIT("take-profit-limit"),
    @Json(name = "settle-position") SETTLE_POSITION("settle-position")
    ;

    companion object {
        fun from(code: String) =
            values().find { it.code == code } ?: throw IllegalArgumentException("Unknown order type  $code")
    }
}

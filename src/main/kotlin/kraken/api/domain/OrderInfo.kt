package kraken.api.domain

import java.math.BigDecimal
import java.time.Instant

data class OrderInfo(
    val closetm: Instant?,
    val cost: BigDecimal?,
    val descr: OrderDescription,
    val expiretm: Instant?,
    val fee: BigDecimal?,
    val limitprice: BigDecimal?,
    val oflags: String?,
    val opentm: Instant,
    val price: BigDecimal?,
    val refid: String?,
    val starttm: Instant?,
    val status: KrakenOrderStatus,
    val stopprice: BigDecimal?,
    val userref: Long?,
    val vol: BigDecimal?,
    val vol_exec: BigDecimal?,
    val trades: List<String>?
)

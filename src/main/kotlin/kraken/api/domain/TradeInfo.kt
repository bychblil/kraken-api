package kraken.api.domain

import java.math.BigDecimal
import java.time.Instant

data class TradeInfo(
    val tradeId: String,
    val orderTxId: String,
    val posTxId: String?,
    val assetPair: String,
    val time: Instant, // timestamp of trade
    val type: KrakenTradeType,
    val orderType: KrakenOrderType,
    val price: BigDecimal,
    val cost: BigDecimal,
    val fee: BigDecimal,
    val volume: BigDecimal,
    val margin: BigDecimal?,
    val misc: String?
)

package kraken.api.ws

enum class ErrorCode(val code: Set<String>) {
    AUTH_ACCOUNT_TEMPORARY_DISABLED(setOf("EAuth:Account temporary disabled")),
    AUTH_RATE_LIMIT_EXCEEDED(setOf("EAuth:Rate limit exceeded")),
    AUTH_TOO_MANY_REQUESTS(setOf("EAuth:Too many requests")),
    GENERAL_INTERNAL_ERROR(setOf("EGeneral:Internal error")),
    ORDER_CANNOT_OPEN_POSITION(setOf("EOrder:Cannot open")),
    ORDER_INSUFFICIENT_FUNDS(setOf("EOrder:Insufficient funds")),
    ORDER_INVALID_PRICE(setOf("EOrder:Invalid price")),
    ORDER_MINIMUM_NOT_MET(
        setOf(
            "EOrder:Order minimum not met",
            "EGeneral:Invalid arguments:volume"
        )
    ),
    ORDER_ORDERS_LIMIT_EXCEEDED(setOf("EOrder:Orders limit exceeded")),
    ORDER_POSITIONS_LIMIT_EXCEEDED(setOf("EOrder:Positions limit exceeded")),
    ORDER_RATE_LIMIT_EXCEEDED(setOf("EOrder:Rate limit exceeded")),
    ORDER_SCHEDULED_LIMIT_EXCEEDED(setOf("EOrder:Scheduled orders limit exceeded")),
    ORDER_UNKNOWN_POSITION(setOf("EOrder:Unknown position")),
    SERVICE_MARKET_CANCEL_ONLY(setOf("EService:Market in cancel_only mode")),
    SERVICE_MARKET_LIMIT_ONLY(setOf("EService:Market in limit_only mode")),
    SERVICE_MARKET_POST_ONLY(setOf("EService:Market in post_only mode")),
    SERVICE_UNAVAILABLE(setOf("EService:Unavailable"))
    ;

    companion object {
        fun fromErrorMessage(message: String): ErrorCode? =
            values().filter { it.code.stream().anyMatch { code -> code.startsWith(message) } }.firstOrNull()
    }
}

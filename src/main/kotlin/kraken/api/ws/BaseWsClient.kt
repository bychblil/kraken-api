package kraken.api.ws

import com.google.common.cache.Cache
import com.google.common.cache.CacheBuilder
import com.google.common.cache.RemovalCause
import com.google.common.cache.RemovalNotification
import kotlinx.coroutines.Job
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive
import kotlinx.serialization.json.jsonPrimitive
import kraken.api.KrakenPublicClientImpl
import kraken.api.infra.schedule
import kraken.api.ws.data.HeartbeatMessage

import kraken.api.ws.data.KrakenMessage
import kraken.api.ws.data.KrakenRequest
import kraken.api.ws.data.KrakenResponse
import kraken.api.ws.data.OrderStatus
import kraken.api.ws.data.SubscriptionStatusMessage
import kraken.api.ws.data.SystemStatusMessage
import org.http4k.websocket.Websocket
import org.http4k.websocket.WsMessage
import org.java_websocket.exceptions.WebsocketNotConnectedException
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.Duration
import java.time.Instant
import java.util.concurrent.TimeUnit

/**
 * BaseWs client.
 */
open class BaseWsClient {

    protected fun resolveChannel(payload: String) : Channel {
        when (val element = Json.parseToJsonElement(payload)) {
            // if it is object, look into event field
            is JsonObject -> return Channel.from(element[EVENT]!!.jsonPrimitive.content)
            // if it is json array, try to find string element with channelName
            is JsonArray -> return Channel.values().find { element.contains(JsonPrimitive(it.channelName)) } ?: throw IllegalArgumentException("None supported channel found in $element")
        }

        throw IllegalArgumentException("Unknown channel for $payload")
    }

    fun sendWsReqWithReconnectCheck(ws: Websocket, payload: String, reconnect: () -> Unit): Boolean {
        try {
            ws.send(WsMessage(payload))
        } catch (notConnected: WebsocketNotConnectedException) {
            // nok
            reconnect()
            return false
        }
        // ok
        return true
    }

    companion object {
        const val EVENT = "event"
    }
}

object SystemStatusHandlerImpl : SystemStatusHandler {
    override fun handleMessage(message: SystemStatusMessage) {
        if (!message.isOnline()) KrakenPublicClientImpl.log.error("Kraken System status is reported as {}, unexpected behavior may occur!", message.status)
    }
}

class HeartbeatHandlerImpl : HeartbeatHandler {
    val log: Logger = LoggerFactory.getLogger(HeartbeatHandlerImpl::class.java)
    var lastReceived: Instant = Instant.now()

    override fun handleMessage(message: HeartbeatMessage) {
        log.trace("Heartbeat received.")
        lastReceived = Instant.now()
    }

    fun reset() {
        lastReceived = Instant.now()
    }
}

object SubscriptionStatusHandlerImpl : SubscriptionStatusHandler {
    val log: Logger = LoggerFactory.getLogger(SubscriptionStatusHandlerImpl::class.java)

    override fun handleMessage(message: SubscriptionStatusMessage) {
       log.debug("SubscriptionStatus for ${message.channelName} status: ${message.status}")
    }
}

/**
 * Base handler for request-response over websocket.
 * Does store requests in cache and waits for response.
 */
open class BaseRequestReplyMessageHandler<REQ : KrakenRequest, RESP : KrakenResponse>(
    requestReplyTimeout: Long,
    val log: Logger,
    val expiredMessage : (reqid: Long, status: OrderStatus, errorMessage: String) -> RESP
) {
    // cache
    val requestReplyCache: Cache<Long, kotlinx.coroutines.channels.Channel<KrakenMessage>> = CacheBuilder.newBuilder()
        .expireAfterWrite(requestReplyTimeout, TimeUnit.SECONDS)
        .removalListener<Long, kotlinx.coroutines.channels.Channel<KrakenMessage>> { handleExpired(it) }
        .build<Long, kotlinx.coroutines.channels.Channel<KrakenMessage>>()
    // maintenance job
    var cacheMaintenance: Job? = null

    fun registerRequest(request: REQ) : kotlinx.coroutines.channels.Channel<KrakenMessage> {
        // lazy start cache updater
        if (null == cacheMaintenance) {
            log.trace("Lazy starting cache maintenance job")
            cacheMaintenance = schedule(Duration.ofSeconds(1L)) { requestReplyCache.cleanUp() }
        }
        // create channel and save it
        val channel = kotlinx.coroutines.channels.Channel<KrakenMessage>(capacity = 1)
        requestReplyCache.put(request.reqid, channel)

        // return
        return channel
    }

    fun handleMessageInternal(message: RESP) {
        log.trace("Handle message: {}", message)
        val identification = message.reqid
        // try to find channel for this reqid
        val channel = requestReplyCache.asMap()[identification]
        log.trace("Found waiting channel {}", channel)
        // offer to channel
        channel?.offer(message)
        // invalidate entry in cache, as we do not need it anymore
        requestReplyCache.invalidate(identification)
    }
    /**
     * Handle expired - meaning timeout.
     */
    private fun handleExpired(notification: RemovalNotification<Long, kotlinx.coroutines.channels.Channel<KrakenMessage>>) {
        if (RemovalCause.EXPIRED == notification.cause) {
            try {
                val channel = notification.value
                // error response for caller
                log.trace("Timeout exceeded for request reqid: {}", notification.key)
                channel.offer(expiredMessage.invoke(notification.key, OrderStatus.UNKNOWN, "Timeout when waiting for response to request"))
            } catch (t: Throwable) {
                // just log, do not propagate
                log.info("Unable to properly process expired request :", t)
            }
        }
    }
}

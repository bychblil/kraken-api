package kraken.api.ws

/**
 * List of supported channels that can be subscribed to.
 */
enum class Channel(val channelName: String) {
    Ticker("ticker"),
    Heartbeat("heartbeat"),
    SubscriptionStatus("subscriptionStatus"),
    SystemStatus("systemStatus"),
    Pong("pong"),
    OwnTrades("ownTrades"),
    OpenOrders("openOrders"),
    AddOrderStatus("addOrderStatus"),
    CancelOrderStatus("cancelOrderStatus"),
    ;

    companion object {
        fun from(channelName: String) =
            values().find { it.channelName == channelName } ?: throw IllegalArgumentException("No channel with name $channelName")
    }
}

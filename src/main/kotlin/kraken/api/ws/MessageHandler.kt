package kraken.api.ws

import kraken.api.ws.data.AddOrderStatusMessage
import kraken.api.ws.data.CancelOrderStatusMessage
import kraken.api.ws.data.HeartbeatMessage
import kraken.api.ws.data.KrakenMessage
import kraken.api.ws.data.OpenOrdersMessage
import kraken.api.ws.data.OwnTradesMessage
import kraken.api.ws.data.PingPongMessage
import kraken.api.ws.data.SubscriptionStatusMessage
import kraken.api.ws.data.SystemStatusMessage
import kraken.api.ws.data.TickerMessage

interface SystemStatusHandler: MessageHandler<SystemStatusMessage>
interface TickerHandler: MessageHandler<TickerMessage>
interface PongHandler: MessageHandler<PingPongMessage>
interface OwnTradesHandler: MessageHandler<OwnTradesMessage>
interface OpenOrdersHandler: MessageHandler<OpenOrdersMessage>
interface AddOrderStatusHandler: MessageHandler<AddOrderStatusMessage>
interface CancelOrderStatusHandler: MessageHandler<CancelOrderStatusMessage>
interface HeartbeatHandler: MessageHandler<HeartbeatMessage>
interface SubscriptionStatusHandler: MessageHandler<SubscriptionStatusMessage>

interface MessageHandler <T : KrakenMessage> {
    fun handleMessage(message: T)
}

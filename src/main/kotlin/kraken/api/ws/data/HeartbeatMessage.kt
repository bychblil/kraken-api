package kraken.api.ws.data

/**
 * Dummy heartbeat message.
 */
class HeartbeatMessage : KrakenMessage() {

    companion object {
        val instance = HeartbeatMessage()
    }
}

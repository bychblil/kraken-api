package kraken.api.ws.data

/**
 * Response from kraken WS.
 */
open class KrakenMessage

/**
 * Request, having reqid field.
 */
open class KrakenRequest(val reqid: Long): KrakenMessage()

/**
 * Response, having reqid field.
 */
open class KrakenResponse(val reqid: Long): KrakenMessage()

package kraken.api.ws.data

import com.squareup.moshi.JsonAdapter
import kraken.api.infra.moshi

data class PingPongMessage(
    val event: String = PING,
    val reqid: Long?
) : KrakenMessage() {

    companion object {
        const val PING = "ping"
        val jsonAdapter: JsonAdapter<PingPongMessage> = moshi.adapter(PingPongMessage::class.java)
        fun toKotlinObject(str : String) : PingPongMessage = jsonAdapter.fromJson(str)!!
    }
}

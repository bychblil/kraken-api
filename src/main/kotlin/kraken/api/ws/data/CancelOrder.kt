package kraken.api.ws.data

data class CancelOrder(
    val txIds: Set<String>
) {
    companion object {
        fun toRequest(order: CancelOrder, reqid: Long, token: String) = CancelOrderRequest(reqid = reqid, token = token, txid = order.txIds)
    }
}

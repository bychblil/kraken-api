package kraken.api.ws.data

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Types
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.jsonArray
import kotlinx.serialization.json.jsonObject
import kraken.api.domain.KrakenOrderType
import kraken.api.domain.TradeInfo
import kraken.api.domain.KrakenTradeType
import kraken.api.infra.moshi
import java.math.BigDecimal
import java.time.Instant
import java.util.Collections

class OwnTradesMessage (
    val trades: List<TradeInfo>
): KrakenMessage() {

    private data class TradeContent(
        val cost: BigDecimal,
        val fee: BigDecimal,
        val margin: BigDecimal,
        val ordertxid: String, // TDLH43-DVQXD-2KHVYY
        val ordertype: String, // limit
        val pair: String,
        val postxid: String?,
        val price: BigDecimal,
        val time: Instant, // 1560516023.070651
        val type: String, // sell/buy
        val vol: BigDecimal,
        val misc: String?
    )

    companion object {
        private val tradeContentAdapter: JsonAdapter<Map<String, TradeContent>> = moshi.adapter(Types.newParameterizedType(Map::class.java, String::class.java, TradeContent::class.java))

        fun toKotlinObject(str : String) : OwnTradesMessage {
            val payloadArray = Json.parseToJsonElement(str).jsonArray
            if (payloadArray.size < 3) {
                throw IllegalArgumentException("Cannot process OwnTrades response $payloadArray")
            }
            if (payloadArray[0].jsonArray.isEmpty()) {
                // empty list of trades
                return OwnTradesMessage(trades = Collections.emptyList())
            }
            // parse map of trades
            val content = tradeContentAdapter.fromJson(payloadArray[0].jsonArray[0].jsonObject.toString())!!

            val tradeList = content.map {
                TradeInfo(tradeId = it.key, cost = it.value.cost, fee = it.value.fee, margin = it.value.margin,
                    orderTxId = it.value.ordertxid, orderType = KrakenOrderType.from(it.value.ordertype), assetPair = it.value.pair,
                    posTxId = it.value.postxid, price = it.value.price, time = it.value.time,
                    type = KrakenTradeType.from(it.value.type), volume = it.value.vol, misc = it.value.misc)
            }
            return OwnTradesMessage(trades = tradeList)
        }
    }
}

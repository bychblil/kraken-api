package kraken.api.ws.data

import com.squareup.moshi.JsonAdapter
import kraken.api.infra.moshi

class CancelOrderRequest(
    val event: String = CancelOrderRequest.EVENT,
    val token: String? = null,
    reqid: Long,
    val txid: Collection<String>
) : KrakenRequest(reqid = reqid) {
    companion object {
        val jsonAdapter: JsonAdapter<CancelOrderRequest> = moshi.adapter(CancelOrderRequest::class.java)
        const val EVENT = "cancelOrder"
    }
}

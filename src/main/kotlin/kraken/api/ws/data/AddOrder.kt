package kraken.api.ws.data

import kraken.api.domain.KrakenOrderType
import kraken.api.domain.KrakenTradeType
import java.math.BigDecimal

data class AddOrder (
    val orderType: KrakenOrderType, // enum  market|limit|stop-loss|take-profit|stop-loss-limit|take-profit-limit|settle-position
    val tradeType: KrakenTradeType, // enum Side, buy or sell
    val pair: String, // Currency pair
    val price: BigDecimal? = null, // Optional dependent on order type - order price
    val price2: BigDecimal? = null, // dependent on order type - order secondary price
    val volume: BigDecimal, // order volume in lots
    val leverage: BigDecimal? = null, // amount of leverage desired (optional; default = none)
    val oflags: String? = null, // comma delimited list of order flags.
    val startTimeInSeconds: Long? = null, // scheduled start time in seconds, now is default
    val expireTimeInSeconds: Long? = null, // order expire time in seconds, no expiration is default
    val userref: Long?, // user reference ID, should be integer as string
    val validateOnly: Boolean = false // validate inputs only, do not submit order; dry run
) {

    companion object {
        fun toRequest(order: AddOrder, reqid: Long, token: String) = AddOrderRequest(reqid = reqid, token = token, ordertype = order.orderType.code,
            type = order.tradeType.code, pair = order.pair, price = order.price, price2 = order.price2, volume = order.volume, leverage = order.leverage,
            oflags = order.oflags, starttm = order.startTimeInSeconds?.toString(), expiretm = order.expireTimeInSeconds?.toString(), userref = order.userref?.toString(),
            validate = if (order.validateOnly) AddOrderRequest.VALIDATE_ONLY else null)
    }
}

package kraken.api.ws.data

import com.squareup.moshi.JsonAdapter
import kraken.api.infra.moshi

data class SubscribeRequest(
    val event: String,
    val reqid: Long? = null,
    val pair: Collection<String>? = null,
    val subscription: Subscription
) {

    data class Subscription(
        val interval: Long? = null,
        val name: String, // enum
        var token: String? = ""
    )

    companion object {
        val jsonAdapter: JsonAdapter<SubscribeRequest> = moshi.adapter(SubscribeRequest::class.java)
    }
}

package kraken.api.ws.data

import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Types
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.jsonArray
import kotlinx.serialization.json.jsonObject
import kraken.api.domain.KrakenOrderStatus
import kraken.api.infra.moshi
import java.math.BigDecimal
import java.time.Instant
import java.util.Collections

/**
 * API desciption:
 * Open orders. Feed to show all the open orders.
 * Initial snapshot will provide list of all open orders and then any updates to the open orders list will be sent.
 */
class OpenOrdersMessage (
    val orders: List<OpenOrder>
): KrakenMessage() {

    data class OpenOrder(
        val orderId: String,
        val avgPrice: BigDecimal?,
        val cost: BigDecimal?,
        val fee: BigDecimal?,
        val lastUpdated: Instant?,
        val status: KrakenOrderStatus,
        val volExec: BigDecimal?
    )

    // used for serialization/deserialization
    private data class OpenOrderPayload(
        val avg_price: BigDecimal?,
        val cost: BigDecimal?,
        val fee: BigDecimal?,
        val lastupdated: Instant?,
        val status: KrakenOrderStatus?,
        val vol_exec: BigDecimal?
    )

    companion object {
        private val openOrdersContentAdapter: JsonAdapter<Map<String, OpenOrderPayload>> = moshi.adapter(Types.newParameterizedType(Map::class.java, String::class.java, OpenOrderPayload::class.java))

        fun toKotlinObject(str : String) : OpenOrdersMessage {
            val payloadArray = Json.parseToJsonElement(str).jsonArray
            if (payloadArray.size < 3) {
                throw IllegalArgumentException("Cannot process OpenOrders response $payloadArray")
            }
            if (payloadArray[0].jsonArray.isEmpty()) {
                // empty list of orders, nothing more
                return OpenOrdersMessage(orders = Collections.emptyList())
            }

            // parse map of trades
            val content = openOrdersContentAdapter.fromJson(payloadArray[0].jsonArray[0].jsonObject.toString())!!

            val ordersList = content
                .filter { null != it.value.status } // filter out orders without state
                .map { OpenOrder(orderId = it.key, avgPrice = it.value.avg_price, cost = it.value.cost, fee = it.value.fee,
                    lastUpdated = it.value.lastupdated, status = it.value.status!!, volExec = it.value.vol_exec)
            }
            return OpenOrdersMessage(orders = ordersList)
        }
    }
}

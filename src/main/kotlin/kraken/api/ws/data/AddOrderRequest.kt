package kraken.api.ws.data

import com.squareup.moshi.JsonAdapter
import kraken.api.infra.moshi
import java.math.BigDecimal

class AddOrderRequest (
    val event: String = EVENT,
    val token: String? = null,
    reqid: Long,
    val ordertype: String, // enum  market|limit|stop-loss|take-profit|stop-loss-limit|take-profit-limit|settle-position
    val type: String, // enum Side, buy or sell
    val pair: String, // Currency pair
    val price: BigDecimal? = null, // Optional dependent on order type - order price
    val price2: BigDecimal? = null, // dependent on order type - order secondary price
    val volume: BigDecimal, // order volume in lots
    val leverage: BigDecimal? = null, // amount of leverage desired (optional; default = none)
    val oflags: String? = null, // comma delimited list of order flags.
    val starttm: String? = null, // scheduled start time in seconds, now is default
    val expiretm: String? = null, // order expire time in seconds, no expiration is default
    val userref: String?, // user reference ID, should be integer as string
    val validate: String? = null, // validate inputs only, do not submit order; dry run when set to something like true
    val trading_agreement: String = AGREE
) : KrakenRequest(reqid = reqid) {
    companion object {
        val jsonAdapter: JsonAdapter<AddOrderRequest> = moshi.adapter(AddOrderRequest::class.java)
        const val VALIDATE_ONLY = "true"
        const val AGREE = "agree"
        const val EVENT = "addOrder"
    }
}

// TODO 14.12. missing three arguments with troublesome naming close[ordertype], close[price], close[price2]

package kraken.api.ws.data

import com.squareup.moshi.JsonAdapter
import kraken.api.infra.moshi

data class SystemStatusMessage(
    val connectionID: String?, // gets lost sometimes
    val status: String,
    val version: String
) : KrakenMessage() {

    fun isOnline() : Boolean = this.status == ONLINE

    companion object {
        const val ONLINE = "online"
        val systemStatusAdapter: JsonAdapter<SystemStatusMessage> = moshi.adapter(SystemStatusMessage::class.java)
        fun toKotlinObject(str : String) : SystemStatusMessage = systemStatusAdapter.fromJson(str)!!
    }
}

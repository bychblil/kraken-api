package kraken.api.ws.data

enum class OrderStatus(val statusCode: String) {
    OK("ok"), // from api
    ERROR("error"), // from api
    UNKNOWN("unknown") // technical error of some kind
    ;

    companion object {
        fun from(statusCode: String) =
            values().find { it.statusCode == statusCode } ?: throw IllegalArgumentException("Unknown status for addOrder request $statusCode")
    }
}

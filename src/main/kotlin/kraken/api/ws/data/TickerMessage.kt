package kraken.api.ws.data

import com.squareup.moshi.JsonAdapter
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.jsonArray
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive
import kraken.api.infra.moshi
import java.math.BigDecimal

data class TickerMessage (
    val ask: Price,
    val bid: Price,
    val close: Price,
    val volume: Value,
    val volumeWeighted: Value, // Volume weighted average price
    val trades: Value, // number of trades
    val lowPrice: Value,
    val highPrice: Value,
    val openPrice: Value,
    val assetPair: String
) : KrakenMessage() {

    data class Price(
        val price: BigDecimal,
        val wholeLotVolume: BigDecimal? = null,
        val lotVolume: BigDecimal
    )

    data class Value(
        val today: BigDecimal,
        val last24h: BigDecimal
    ) {
        companion object {
            fun fromList(list : List<BigDecimal>) = Value(today = list[0], last24h = list[1])
        }
    }

    private data class TickerContent(
        val a: List<BigDecimal>,
        val b: List<BigDecimal>,
        val c: List<BigDecimal>,
        val v: List<BigDecimal>,
        val p: List<BigDecimal>,
        val t: List<BigDecimal>,
        val l: List<BigDecimal>,
        val h: List<BigDecimal>,
        val o: List<BigDecimal>
    )

    companion object {
        private val tickerContentAdapter: JsonAdapter<TickerContent> = moshi.adapter(TickerContent::class.java)

        fun toKotlinObject(str : String) : TickerMessage {
            val payloadArray = Json.parseToJsonElement(str).jsonArray
            if (payloadArray.size < 4) {
                throw IllegalArgumentException("Cannot process Ticker response $payloadArray")
            }
            val content = tickerContentAdapter.fromJson(payloadArray[1].jsonObject.toString())!!
            val assetPair = payloadArray[3].jsonPrimitive.content

            val askPrice = Price(price = content.a[0], wholeLotVolume = content.a[1], lotVolume = content.a[2])
            val bidPrice = Price(price = content.b[0], wholeLotVolume = content.b[1], lotVolume = content.b[2])
            val closePrice = Price(price = content.c[0], lotVolume = content.c[1])

            return TickerMessage(assetPair = assetPair, ask = askPrice, bid = bidPrice, close = closePrice, volume = Value.fromList(content.v),
                volumeWeighted = Value.fromList(content.p), trades = Value.fromList(content.t), lowPrice = Value.fromList(content.l),
                highPrice = Value.fromList(content.h), openPrice = Value.fromList(content.o))
        }
    }
}

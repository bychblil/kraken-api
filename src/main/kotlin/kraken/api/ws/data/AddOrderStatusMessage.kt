package kraken.api.ws.data

import com.squareup.moshi.JsonAdapter
import kraken.api.infra.moshi
import kraken.api.ws.ErrorCode

class AddOrderStatusMessage(
    reqid: Long,
    val status: OrderStatus,
    val txid: String?,
    val descr: String?,
    val errorMessage: String?,
    val errorCode: ErrorCode? = null
) : KrakenResponse(reqid = reqid) {

    private data class AddOrderResponseContent(
        val reqid: Long,
        val status: String,
        val txid: String?,
        val descr: String?,
        val errorMessage: String?
    )

    companion object {
        private val addOrderResponseContentAdapter: JsonAdapter<AddOrderResponseContent> = moshi.adapter(AddOrderResponseContent::class.java)

        fun toKotlinObject(str : String) : AddOrderStatusMessage {
            val content = addOrderResponseContentAdapter.fromJson(str)!!
            return AddOrderStatusMessage(reqid = content.reqid, status = OrderStatus.from(content.status), txid = content.txid,
                descr = content.descr, errorMessage = content.errorMessage, errorCode = if (null != content.errorMessage) ErrorCode.fromErrorMessage(content.errorMessage) else null)
        }
    }
}

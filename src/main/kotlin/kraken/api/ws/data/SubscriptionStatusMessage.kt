package kraken.api.ws.data

import com.squareup.moshi.JsonAdapter
import kraken.api.infra.moshi

/**
 * Subscription status.
 */
data class SubscriptionStatusMessage(
    val channelName: String?,
    val status: String
): KrakenMessage() {

    companion object {
        val adapter: JsonAdapter<SubscriptionStatusMessage> = moshi.adapter(SubscriptionStatusMessage::class.java)
        fun toKotlinObject(str : String) : SubscriptionStatusMessage = adapter.fromJson(str)!!
    }
}

package kraken.api.ws.data

import com.squareup.moshi.JsonAdapter
import kraken.api.infra.moshi
import kraken.api.ws.ErrorCode

class CancelOrderStatusMessage (
    reqid: Long,
    val status: OrderStatus,
    val errorMessage: String?,
    val errorCode: ErrorCode? = null
) : KrakenResponse(reqid = reqid) {

    private data class CancelOrderStatusResponseContent(
        val reqid: Long?,
        val status: String,
        val errorMessage: String?
    )

    companion object {
        private val jsonAdapter: JsonAdapter<CancelOrderStatusResponseContent> = moshi.adapter(CancelOrderStatusResponseContent::class.java)
        fun toKotlinObject(str : String) : CancelOrderStatusMessage {
            val content = jsonAdapter.fromJson(str)!!
            return CancelOrderStatusMessage(reqid = content.reqid!!, errorMessage = content.errorMessage, status = OrderStatus.from(content.status),
                errorCode = if (null != content.errorMessage) ErrorCode.fromErrorMessage(content.errorMessage) else null)
        }
    }
}

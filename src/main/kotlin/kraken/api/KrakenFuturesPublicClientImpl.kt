package kraken.api

import com.squareup.moshi.JsonAdapter
import kraken.api.http.data.response.FuturesFeeScheduleResponse
import kraken.api.http.data.response.InstrumentsResponse
import kraken.api.infra.moshi
import kraken.api.infra.verifyHttpStatusResponse
import kraken.api.ws.BaseWsClient
import org.http4k.client.OkHttp
import org.http4k.core.Method.GET
import org.http4k.core.Request
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.Duration

/** Public endpoints - https://support.kraken.com/hc/en-us/sections/360003562371-Websocket-API-Public */
class KrakenFuturesPublicClientImpl(
    httpUrl: String = "https://futures.kraken.com/derivatives",
    val webSocketUrl: String = "wss://futures.kraken.com/ws/v1",
    val connectionCheckInterval: Duration = Duration.ofSeconds(60),
    val connectionCheckTimeout: Duration = Duration.ofSeconds(5),
    val reconnectAttemptInterval: Duration = Duration.ofSeconds(140),
) : KrakenFuturesPublicClient, BaseWsClient() {

    /** API endpoints */
    private val FEE_SCHEDULE = "$httpUrl/api/v3/feeschedules"
    private val INSTRUMENTS = "$httpUrl/api/v3/instruments"

    /**  Adapters for serialization */
    private val feeScheduleResponseAdapter: JsonAdapter<FuturesFeeScheduleResponse> = moshi.adapter(FuturesFeeScheduleResponse::class.java)
    private val instrumentsResponseAdapter: JsonAdapter<InstrumentsResponse> = moshi.adapter(InstrumentsResponse::class.java)

    private val http by lazy { OkHttp() }

    override fun feeSchedule() : FuturesFeeScheduleResponse = http(Request(GET, FEE_SCHEDULE))
        .also { verifyHttpStatusResponse(it, log) }
        .let { feeScheduleResponseAdapter.fromJson(it.bodyString())!! }

    override fun instruments(): InstrumentsResponse = http(Request(GET, INSTRUMENTS))
        .also { verifyHttpStatusResponse(it, log) }
        .let { instrumentsResponseAdapter.fromJson(it.bodyString())!! }

    companion object {
        val log: Logger = LoggerFactory.getLogger(KrakenFuturesPublicClientImpl::class.java)
        const val SUBSCRIBE = "subscribe"
        const val RECONNECT_DELAY_MS = 10_000L
    }
}

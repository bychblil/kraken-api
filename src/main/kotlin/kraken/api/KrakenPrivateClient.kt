package kraken.api

import kraken.api.domain.OrderInfo
import kraken.api.domain.TradeInfo
import kraken.api.domain.TradeVolumeInfo
import kraken.api.http.data.Balance
import kraken.api.http.data.ClosedOrdersFilter
import kraken.api.http.data.QueryOrdersFilter
import kraken.api.http.data.QueryTradesFilter
import kraken.api.http.data.TradeBalance
import kraken.api.http.data.TradeHistoryFilter
import kraken.api.http.data.WebSocketToken
import kraken.api.http.data.response.ClosedOrdersResponse
import kraken.api.ws.Channel
import kraken.api.ws.MessageHandler
import kraken.api.ws.data.AddOrder
import kraken.api.ws.data.AddOrderStatusMessage
import kraken.api.ws.data.CancelOrder
import kraken.api.ws.data.CancelOrderStatusMessage

/**
 * Kraken private endpoints - see https://www.kraken.com/features/api#private-user-data
 */
interface KrakenPrivateClient {

    /**
     * Get account balances, can be empty if none are non-zero.
     */
    fun getAccountBalances(): Collection<Balance>

    /**
     * Get trade balance, assetCode is base asset to determine balance in.
     */
    fun getTradeBalance(assetCode: String): TradeBalance

    /**
     * Get web socket token.
     */
    fun getWebSocketToken() : WebSocketToken

    /**
     * Get closed orders.
     */
    fun getClosedOrders(filter: ClosedOrdersFilter) : ClosedOrdersResponse

    /**
     * Query orders.
     * Response is map of orders, key is kraken txId.
     */
    fun queryOrdersInfo(filter: QueryOrdersFilter) : Map<String, OrderInfo>

    /**
     * Trades history.
     */
    fun tradesHistory(filter: TradeHistoryFilter): Map<String, TradeInfo>

    /**
     * Query trades.
     * Response is map of traders, key is kraken txId.
     */
    fun queryTradesInfo(filter: QueryTradesFilter): Map<String, TradeInfo>

    /**
     * Trade volume, with info about current fees.
     */
    fun getTradeVolume(pairs: List<String>): TradeVolumeInfo

    /**
     * Add order. Asynchronous operation, needs to be subscribed prior.
     * #see subscribe
     */
    suspend fun addOrder(order: AddOrder) : AddOrderStatusMessage

    /**
     * Cancel order (or multiple). Asynchronous operation, needs to be subscribed prior.
     * #see subscribe
     */
    suspend fun cancelOrder(order: CancelOrder) : CancelOrderStatusMessage

    /**
     * Subscribe to asynchronous messages.
     */
    fun subscribe(
        channels: Set<Channel>,
        handlers: Collection<MessageHandler<*>> = emptyList()
    )

    /**
     * Rate limit to set, should be set by account type.
     */
    data class RateLimit(val countMaximum: Long, val decreaseByOneInSeconds: Long)

    /**
     * Consumer for metrics.
     */
    interface KrakenMetricsConsumer {
        /**
         * Will be called for each query to Kraken API.
         */
        fun increaseQueryCount()

        /**
         * Set current rate limit value whenever it is evaluated.
         */
        fun setCurrentRateLimit(rateLimit: Int)
    }
}

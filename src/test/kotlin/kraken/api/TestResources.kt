package kraken.api

class TestResources

/**
 * Read file from test classpath.
 */
fun readFile(fileName: String)
    = TestResources::class.java.getResource(fileName).readText(Charsets.UTF_8)

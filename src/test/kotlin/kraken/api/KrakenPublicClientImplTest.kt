package kraken.api

import kraken.api.http.data.Asset
import kraken.api.http.data.AssetPair
import kraken.api.http.data.OrderBook
import kraken.api.http.exception.HttpStatusException
import kraken.api.infra.comparedEquals
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.mockserver.model.HttpRequest.request
import org.mockserver.model.HttpResponse.response
import org.mockserver.model.MediaType
import java.math.BigDecimal

class KrakenPublicClientImplTest : MockServerTestBase() {

    // tested client
    private val client = KrakenPublicClientImpl(httpUrl = mockServerUrl, webSocketUrl = mockServerUrl)

    @Test
    fun getAssets_ok() {
        // prepare
        mockServer.setup(request().withMethod("GET").withPath("/0/public/Assets"))
                .respond(response()
                    .withStatusCode(200)
                    .withContentType(MediaType.APPLICATION_JSON_UTF_8)
                    .withBody(readFile("/public/get-assets.json")))

        val result: List<Asset> = client.getAssets()
        assertNotNull(result, "result must not be null")
        assertFalse(result.isEmpty(), "result list should not be empty")
        val asset = result[0]
        assertEquals("ADA", asset.id)
        assertEquals(8L, asset.decimals)
    }

    @Test
    fun getAssets_forbidden() {
        mockServer.setup(request().withMethod("GET").withPath("/0/public/Assets"))
                .respond(response()
                        .withStatusCode(403))

        assertThrows(HttpStatusException::class.java) {
            client.getAssets()
        }
    }

    @Test
    fun getAssets_internalError() {
        mockServer.setup(request().withMethod("GET").withPath("/0/public/Assets"))
                .respond(response()
                        .withStatusCode(500)
                        .withBody("Reason why it is down in payload"))

        assertThrows(HttpStatusException::class.java) {
            client.getAssets()
        }
    }

    @Test
    fun getAssetPairs_ok() {
        // prepare
        mockServer.setup(request().withMethod("GET").withPath("/0/public/AssetPairs"))
                .respond(response()
                        .withStatusCode(200)
                        .withContentType(MediaType.APPLICATION_JSON_UTF_8)
                        .withBody(readFile("/public/asset-pairs.json")))

        val result: List<AssetPair> = client.getAssetPairs()
        assertNotNull(result, "result must not be null")
        assertFalse(result.isEmpty(), "result list should not be empty")
        val assetPair = result[0]
        assertEquals("ADAETH", assetPair.id)
        assertEquals(7, assetPair.pair_decimals)
        assertTrue(BigDecimal.valueOf(0.26).comparedEquals(assetPair.fees_trader[0].fee), "fee does not match")
    }

    @Test
    fun getAssetPairs_notFound() {
        mockServer.setup(request().withMethod("GET").withPath("/0/public/AssetPairs"))
                .respond(response()
                        .withStatusCode(404))

        assertThrows(HttpStatusException::class.java) {
            client.getAssets()
        }
    }

    @Test
    fun getOrderBook_ok() {
        // prepare
        mockServer.setup(
            request()
                .withMethod("GET")
                .withPath("/0/public/Depth")
                .withQueryStringParameter("pair", "XBTUSD")
        )
            .respond(response()
                .withStatusCode(200)
                .withContentType(MediaType.APPLICATION_JSON_UTF_8)
                .withBody(readFile("/public/order-book.json")))

        val result: OrderBook = client.getOrderBook("XBTUSD")
        assertNotNull(result, "result must not be null")
        assertFalse(result.asks.isEmpty(), "result list should not be empty")
        assertFalse(result.bids.isEmpty(), "result list should not be empty")
        assertEquals(100, result.asks.size)
        assertEquals(100, result.bids.size)
    }

    @Test
    fun getOrderBook_notFound() {
        mockServer.setup(
            request()
                .withMethod("GET")
                .withPath("/0/public/Depth")
                .withQueryStringParameter("pair", "XBTUSD")
        )
            .respond(response()
                .withStatusCode(404))

        assertThrows(HttpStatusException::class.java) { client.getAssets() }
    }
}

package kraken.api

import io.mockk.junit5.MockKExtension
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.mockserver.client.ForwardChainExpectation
import org.mockserver.client.MockServerClient
import org.mockserver.integration.ClientAndServer
import org.mockserver.model.RequestDefinition
import kotlin.random.Random

/**
 * Base class for mock-server testing
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith(MockKExtension::class)
abstract class MockServerTestBase {

    private val random = Random.Default
    private val port = random.nextInt(50000, 60000)
    var mockServer: MockServerClient = MockServerClient("localhost", port)
    val mockServerUrl = "http://localhost:$port"

    @BeforeAll
    fun prepare() {
        mockServer = ClientAndServer.startClientAndServer(port)
    }

    @BeforeEach
    fun clear() {
        mockServer.reset()
    }

    @AfterAll
    fun tearDown() {
        mockServer.stopAsync()
    }
}

// alias for when, as it is keyword
fun MockServerClient.setup(requestDefinition: RequestDefinition) : ForwardChainExpectation {
    return this.`when`(requestDefinition)
}

package kraken.api

import kraken.api.domain.KrakenOrderType
import kraken.api.domain.KrakenTradeType
import kraken.api.http.data.ClosedOrdersFilter
import kraken.api.http.data.QueryOrdersFilter
import kraken.api.http.data.TradeHistoryFilter
import kraken.api.infra.comparedEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockserver.model.HttpRequest
import org.mockserver.model.HttpResponse
import org.mockserver.model.MediaType
import java.math.BigDecimal

class KrakenPrivateClientImplTest : MockServerTestBase() {

    // tested client
    private val client = KrakenPrivateClientImpl(baseUrl = mockServerUrl,
        apiKey = "0123456789",
        privateKey = "MTIzNDU2Nzg5OTg3NjU0MzIx",
        webSocketUrl = mockServerUrl,
        rateLimit = KrakenPrivateClient.RateLimit(999, 1))

    @Test
    fun getWebSocketToken_ok() {
        // prepare
        mockServer.setup(HttpRequest.request().withMethod("POST").withPath("/0/private/GetWebSocketsToken")
                .withHeader("API-Key", "0123456789"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withContentType(MediaType.APPLICATION_JSON_UTF_8)
                        .withBody(readFile("/private/get-websocket-token.json")))

        val token = client.getWebSocketToken()
        assertEquals("dmNKawc6XOv/V3sFKGjd7PIy/w1Eb+t+0t7M9Y3Jzwc", token.token)
        assertEquals(900L, token.expires)
    }

    @Test
    fun getAccountBalance_ok() {
        // prepare
        mockServer.setup(HttpRequest.request().withMethod("POST").withPath("/0/private/Balance")
                .withHeader("API-Key", "0123456789"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withContentType(MediaType.APPLICATION_JSON_UTF_8)
                        .withBody(readFile("/private/account-balance.json")))

        val balances = client.getAccountBalances()

        assertTrue(4 == balances.size, "there should be 4 balances")
        val balance = balances.first()
        assertEquals("ZEUR", balance.assetId)
        assertTrue(BigDecimal.valueOf(47.7393).comparedEquals(balance.balance))
    }

    @Test
    fun getTradeBalance_ok() {
        // prepare
        mockServer.setup(HttpRequest.request().withMethod("POST").withPath("/0/private/TradeBalance")
                .withHeader("API-Key", "0123456789"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withContentType(MediaType.APPLICATION_JSON_UTF_8)
                        .withBody(readFile("/private/trade-balance.json")))

        val tradeBalance = client.getTradeBalance("ZEUR")
        assertTrue(BigDecimal.valueOf(47.7716).comparedEquals(tradeBalance.eb))
    }

    @Test
    fun getClosedOrders_ok() {
        // prepare
        mockServer.setup(HttpRequest.request().withMethod("POST").withPath("/0/private/ClosedOrders")
                .withHeader("API-Key", "0123456789"))
                .respond(HttpResponse.response()
                        .withStatusCode(200)
                        .withContentType(MediaType.APPLICATION_JSON_UTF_8)
                        .withBody(readFile("/private/closed-orders.json")))

        val filter = ClosedOrdersFilter()
        val response = client.getClosedOrders(filter)
        assertEquals(3L, response.result.count)
        val closedOrder = response.result.closed.entries.first()
        assertEquals("OLEC32-5DJ3J-GYNVB6", closedOrder.key)
        assertTrue(BigDecimal.valueOf(0.50923000).comparedEquals(closedOrder.value.vol))
    }

    @Test
    fun queryOrders_ok() {
        // prepare
        mockServer.setup(HttpRequest.request().withMethod("POST").withPath("/0/private/QueryOrders")
            .withHeader("API-Key", "0123456789"))
            .respond(HttpResponse.response()
                .withStatusCode(200)
                .withContentType(MediaType.APPLICATION_JSON_UTF_8)
                .withBody(readFile("/private/query-orders.json")))

        val filter = QueryOrdersFilter(txIds = setOf("32", "42", "64"))
        val response = client.queryOrdersInfo(filter)
        assertEquals(3, response.size)
        val firstOrder = response.entries.first()
        assertEquals("OLEC32-5DJ3J-GYNVB6", firstOrder.key)
        assertEquals(42, firstOrder.value.userref)
        assertEquals(2, firstOrder.value.trades?.size)
    }

    @Test
    fun tradeHistory_ok() {
        // prepare
        mockServer.setup(HttpRequest.request().withMethod("POST").withPath("/0/private/TradesHistory")
            .withHeader("API-Key", "0123456789"))
            .respond(HttpResponse.response()
                .withStatusCode(200)
                .withContentType(MediaType.APPLICATION_JSON_UTF_8)
                .withBody(readFile("/private/trades-history.json")))

        val filter = TradeHistoryFilter()
        val response = client.tradesHistory(filter)
        assertEquals(2, response.size)
        val firstTrade = response.entries.first()
        assertEquals("TUJDEQ-XGF74-A6IXJP", firstTrade.key)
        assertEquals(KrakenOrderType.LIMIT, firstTrade.value.orderType)
        assertEquals("XXBTZEUR", firstTrade.value.assetPair)
        assertEquals(KrakenTradeType.SELL, firstTrade.value.type)
    }

    @Test
    fun getTradeVolume_ok() {
        // prepare
        mockServer.setup(HttpRequest.request().withMethod("POST").withPath("/0/private/TradeVolume")
            .withHeader("API-Key", "0123456789"))
            .respond(HttpResponse.response()
                .withStatusCode(200)
                .withContentType(MediaType.APPLICATION_JSON_UTF_8)
                .withBody(readFile("/private/get-trade-volume.json")))

        val response = client.getTradeVolume(listOf("XXBTZEUR"))
        assertTrue(BigDecimal.valueOf(21507.3812).comparedEquals(response.volume))
        val fee = response.fees["XXBTZEUR"]?.fee ?: BigDecimal.ZERO
        assertTrue(BigDecimal.valueOf(0.26).comparedEquals(fee))
        val feeMaker = response.fees_maker["XXBTZEUR"]?.fee ?: BigDecimal.ZERO
        assertTrue(BigDecimal.valueOf(0.16).comparedEquals(feeMaker))
    }
}

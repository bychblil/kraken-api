package kraken.api

import io.fabric8.mockwebserver.DefaultMockServer
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withTimeout
import kraken.api.domain.KrakenOrderType
import kraken.api.domain.KrakenTradeType
import kraken.api.ws.Channel
import kraken.api.ws.ErrorCode
import kraken.api.ws.OpenOrdersHandler
import kraken.api.ws.OwnTradesHandler
import kraken.api.ws.data.AddOrder
import kraken.api.ws.data.CancelOrder
import kraken.api.ws.data.OpenOrdersMessage
import kraken.api.ws.data.OrderStatus
import kraken.api.ws.data.OwnTradesMessage
import org.apache.commons.lang3.reflect.FieldUtils
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import java.math.BigDecimal
import java.time.Duration

class KrakenPrivateClientImplWsTest {

    private val timeout: Long = 60000L

    @Test
    fun addOrder_ok() {
        val server = createMockServerForWebSocket()
        val client = createTestedClient(server)

        val timestampSupplier: () -> Long = { 42L }
        FieldUtils.writeField(client, "timestampSupplier", timestampSupplier, true)

        server.expect().get().withPath("/")
            .andUpgradeToWebSocket()
            .open()
            .expect(readFile("/public/ws/subscribe-own-trades.json")).andEmit(readFile("/public/ws/subscription-status.json")).always()
            .expect(readFile("/public/ws/subscribe-open-orders.json")).andEmit(readFile("/public/ws/subscription-status.json")).always()
            .expect(readFile("/public/ws/ping.json")).andEmit(readFile("/public/ws/pong.json")).always()
            .expect(readFile("/private/ws/add-order.json")).andEmit(readFile("/private/ws/add-order-response.json")).once()
            .done()
            .once()

        // subscribe
        subscribeAndWait(client)

        // add order
        val order = AddOrder(orderType = KrakenOrderType.MARKET, pair = "XBT/EUR", volume = BigDecimal("0.1"), tradeType = KrakenTradeType.SELL, userref = 42L)
        runBlocking {
            val out = GlobalScope.async {
                client.addOrder(order)
            }
            // wait for timeout for gitlab runner
            withTimeout(timeMillis = timeout) {
                val addOrderResponse = out.await()
                assertEquals(OrderStatus.OK, addOrderResponse.status)
                assertEquals("sell 0.10000000 XBTEUR @ market", addOrderResponse.descr)
            }
        }
        assertEquals(2, server.requestCount)
    }

    @Test
    fun addOrder_insufficientFunds() {
        val server = createMockServerForWebSocket()
        val client = createTestedClient(server)

        val timestampSupplier: () -> Long = { 42L }
        FieldUtils.writeField(client, "timestampSupplier", timestampSupplier, true)

        server.expect().get().withPath("/")
            .andUpgradeToWebSocket()
            .open()
            .expect(readFile("/public/ws/subscribe-own-trades.json")).andEmit(readFile("/public/ws/subscription-status.json")).always()
            .expect(readFile("/public/ws/subscribe-open-orders.json")).andEmit(readFile("/public/ws/subscription-status.json")).always()
            .expect(readFile("/public/ws/ping.json")).andEmit(readFile("/public/ws/pong.json")).always()
            .expect(readFile("/private/ws/add-order.json")).andEmit(readFile("/private/ws/add-order-insufficient-funds.json")).once()
            .done()
            .once()

        // subscribe
        subscribeAndWait(client)

        // add order
        val order = AddOrder(orderType = KrakenOrderType.MARKET, pair = "XBT/EUR", volume = BigDecimal("0.1"), tradeType = KrakenTradeType.SELL, userref = 42L)
        runBlocking {
            val out = GlobalScope.async {
                client.addOrder(order)
            }
            // wait for timeout for gitlab runner
            withTimeout(timeMillis = timeout) {
                val addOrderResponse = out.await()
                assertEquals(OrderStatus.ERROR, addOrderResponse.status)
                assertEquals(ErrorCode.ORDER_INSUFFICIENT_FUNDS, addOrderResponse.errorCode)
            }
        }
        assertEquals(2, server.requestCount)
    }

    @Test
    fun addOrder_timeout() {
        val server = createMockServerForWebSocket()

        val client = KrakenPrivateClientImpl(
            baseUrl = "http://localhost:${server.port}",
            apiKey = "0123456789",
            privateKey = "MTIzNDU2Nzg5OTg3NjU0MzIx",
            webSocketUrl = "http://localhost:${server.port}",
            rateLimit = KrakenPrivateClient.RateLimit(999, 1),
            requestReplyTimeout = Duration.ofSeconds(2)
        ) // timeout only 2s

        val timestampSupplier: () -> Long = { 42L }
        FieldUtils.writeField(client, "timestampSupplier", timestampSupplier, true)

        server.expect().get().withPath("/")
            .andUpgradeToWebSocket()
            .open()
            .expect(readFile("/public/ws/subscribe-own-trades.json")).andEmit(readFile("/public/ws/subscription-status.json")).always()
            .expect(readFile("/public/ws/subscribe-open-orders.json")).andEmit(readFile("/public/ws/subscription-status.json")).always()
            .expect(readFile("/public/ws/ping.json")).andEmit(readFile("/public/ws/pong.json")).always()
            .expect(readFile("/private/ws/add-order.json")).andEmit(readFile("/public/ws/system-status.json")).once() // not correct response, just status
            .done()
            .once()

        // subscribe
        subscribeAndWait(client)

        // add order
        val order = AddOrder(orderType = KrakenOrderType.MARKET, pair = "XBT/EUR", volume = BigDecimal("0.1"), tradeType = KrakenTradeType.SELL, userref = 42L)
        runBlocking {
            val out = GlobalScope.async {
                client.addOrder(order)
            }
            withTimeout(timeMillis = timeout) {
                val addOrderResponse = out.await()
                assertEquals(OrderStatus.UNKNOWN, addOrderResponse.status)
                assertEquals("Timeout when waiting for response to request", addOrderResponse.errorMessage)
            }
        }
        assertEquals(2, server.requestCount)
    }

    @Test
    fun cancelOrder_ok() {
        val server = createMockServerForWebSocket()
        val client = createTestedClient(server)

        val timestampSupplier: () -> Long = { 42L }
        FieldUtils.writeField(client, "timestampSupplier", timestampSupplier, true)

        server.expect().get().withPath("/")
            .andUpgradeToWebSocket()
            .open()
            .expect(readFile("/public/ws/subscribe-own-trades.json")).andEmit(readFile("/public/ws/subscription-status.json")).always()
            .expect(readFile("/public/ws/subscribe-open-orders.json")).andEmit(readFile("/public/ws/subscription-status.json")).always()
            .expect(readFile("/public/ws/ping.json")).andEmit(readFile("/public/ws/pong.json")).always()
            .expect(readFile("/private/ws/cancel-order.json")).andEmit(readFile("/private/ws/cancel-order-response.json")).once()
            .done()
            .once()

        // subscribe
        subscribeAndWait(client)

        // add order
        val order = CancelOrder(txIds = setOf("TX123456789"))
        runBlocking {
            val out = GlobalScope.async {
                client.cancelOrder(order)
            }
            withTimeout(timeMillis = timeout) {
                val cancelOrderStatusMessage = out.await()
                assertEquals(OrderStatus.OK, cancelOrderStatusMessage.status)
                assertNull(cancelOrderStatusMessage.errorMessage)
            }
        }
        assertEquals(2, server.requestCount)
    }

    @Test
    fun cancelOrder_notOk() {
        val server = createMockServerForWebSocket()
        val client = createTestedClient(server)

        val timestampSupplier: () -> Long = { 42L }
        FieldUtils.writeField(client, "timestampSupplier", timestampSupplier, true)

        server.expect().get().withPath("/")
            .andUpgradeToWebSocket()
            .open()
            .expect(readFile("/public/ws/subscribe-own-trades.json")).andEmit(readFile("/public/ws/subscription-status.json")).always()
            .expect(readFile("/public/ws/subscribe-open-orders.json")).andEmit(readFile("/public/ws/subscription-status.json")).always()
            .expect(readFile("/public/ws/ping.json")).andEmit(readFile("/public/ws/pong.json")).always()
            .expect(readFile("/private/ws/cancel-order.json")).andEmit(readFile("/private/ws/cancel-order-response-nok.json")).once()
            .done()
            .once()

        // subscribe
        subscribeAndWait(client)

        // add order
        val order = CancelOrder(txIds = setOf("TX123456789"))
        runBlocking {
            val out = GlobalScope.async {
                client.cancelOrder(order)
            }
            withTimeout(timeMillis = timeout) {
                val cancelOrderStatusMessage = out.await()
                assertEquals(OrderStatus.ERROR, cancelOrderStatusMessage.status)
                assertEquals("EOrder:Unknown order", cancelOrderStatusMessage.errorMessage)
            }
        }
        assertEquals(2, server.requestCount)
    }

    @Test
    fun ownTrades_Ok() {
        val server = createMockServerForWebSocket()
        val client = createTestedClient(server)

        val timestampSupplier: () -> Long = { 42L }
        FieldUtils.writeField(client, "timestampSupplier", timestampSupplier, true)

        server.expect().get().withPath("/")
            .andUpgradeToWebSocket()
            .open()
            .expect(readFile("/public/ws/subscribe-own-trades.json")).andEmit(readFile("/private/ws/own-trades.json")).always()
            .expect(readFile("/public/ws/subscribe-open-orders.json")).andEmit(readFile("/public/ws/subscription-status.json")).always()
            .expect(readFile("/public/ws/ping.json")).andEmit(readFile("/public/ws/pong.json")).always()
            .done()
            .once()

        val ownTradesHandler = object : OwnTradesHandler {
            var stored: OwnTradesMessage? = null
            override fun handleMessage(message: OwnTradesMessage) {
                stored = message
            }
        }

        // subscribe websocket
        client.subscribe(channels = setOf(Channel.OwnTrades, Channel.OpenOrders), handlers = setOf(ownTradesHandler))
        // wait some time
        Thread.sleep(4000)

        // verify message was processed correctly
        assertNotNull(ownTradesHandler.stored)
    }

    @Test
    fun openOrders_Ok() {
        val server = createMockServerForWebSocket()
        val client = createTestedClient(server)

        val timestampSupplier: () -> Long = { 42L }
        FieldUtils.writeField(client, "timestampSupplier", timestampSupplier, true)

        server.expect().get().withPath("/")
            .andUpgradeToWebSocket()
            .open()
            .expect(readFile("/public/ws/subscribe-own-trades.json")).andEmit(readFile("/public/ws/subscription-status.json")).always()
            .expect(readFile("/public/ws/subscribe-open-orders.json")).andEmit(readFile("/private/ws/open-orders.json")).always()
            .expect(readFile("/public/ws/ping.json")).andEmit(readFile("/public/ws/pong.json")).always()
            .done()
            .once()

        val openOrdersHandler = object : OpenOrdersHandler {
            var stored: OpenOrdersMessage? = null
            override fun handleMessage(message: OpenOrdersMessage) {
                stored = message
            }
        }

        // subscribe websocket
        client.subscribe(channels = setOf(Channel.OwnTrades, Channel.OpenOrders), handlers = setOf(openOrdersHandler))
        // wait some time
        Thread.sleep(4000)

        // verify message was processed correctly
        assertNotNull(openOrdersHandler.stored)
        assertEquals(1, openOrdersHandler.stored?.orders?.size)
    }

    private fun createTestedClient(server: DefaultMockServer): KrakenPrivateClientImpl = KrakenPrivateClientImpl(
        baseUrl = "http://localhost:${server.port}",
        apiKey = "0123456789",
        privateKey = "MTIzNDU2Nzg5OTg3NjU0MzIx",
        webSocketUrl = "http://localhost:${server.port}",
        rateLimit = KrakenPrivateClient.RateLimit(999, 1)
    )

    private fun createMockServerForWebSocket(): DefaultMockServer {
        val server = DefaultMockServer()
        server.start()

        server.expect().withPath("/0/private/GetWebSocketsToken")
            .andReturn(200, readFile("/private/get-websocket-token.json")).always()

        return server
    }

    private fun subscribeAndWait(client: KrakenPrivateClientImpl) {
        // subscribe websocket
        client.subscribe(channels = setOf(Channel.OwnTrades, Channel.OpenOrders))

        // some time to get token and start websocket
        Thread.sleep(2000)
    }
}
